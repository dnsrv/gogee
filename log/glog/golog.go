package glog

import (
	"database/sql"
	"fmt"
	"os"

	_ "modernc.org/sqlite"
)

type Output struct {
	db   *sql.DB
	stmt *sql.Stmt
}

func NewOutputSqlite(filename string) (*Output, error) {
	db, err := sql.Open("sqlite", filename)
	if err != nil {
		return nil, err
	}

	stmt, err := db.Prepare(`
	CREATE TABLE IF NOT EXISTS log (message TEXT, created_at DATETIME DEFAULT CURRENT_TIMESTAMP)`)
	if err != nil {
		return nil, err
	}

	_, err = stmt.Exec()
	if err != nil {
		return nil, err
	}

	stmt, err = db.Prepare("INSERT INTO log(message) VALUES(?)")
	if err != nil {
		return nil, err
	}

	return &Output{
		db:   db,
		stmt: stmt,
	}, nil
}

// implement io.Writer to log into out and stdout
func (o *Output) Write(p []byte) (n int, err error) {

	go func() {
		// check last byte is newline or not and remove it
		if p[len(p)-1] == '\n' {
			p = p[:len(p)-1]
		}

		_, err := o.stmt.Exec(string(p))
		if err != nil {
			fmt.Println(err)
		}
	}()

	// write into stdout
	return os.Stdout.Write(p)
}

func (o *Output) Close() error {
	o.stmt.Close()
	return o.db.Close()
}
