package xlog

import (
	"crypto/md5"
	"crypto/rand"
	"database/sql"
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	_ "modernc.org/sqlite"
)

func TestEngineSqlite(t *testing.T) {

	migration := `
		CREATE TABLE IF NOT EXISTS logs (
			id 			INTEGER PRIMARY KEY AUTOINCREMENT, 
			namespace	VARCHAR(128) NULL,
			level       CHAR(3) NOT NULL CHECK (level in ('inf', 'dbg', 'err')),
			request_id	CHAR(36) NULL,
			text 	    VARCHAR(256) NOT NULL,
			is_exported INT NULL,
			created_at 	DATETIME     NULL DEFAULT CURRENT_TIMESTAMP
		);
	`

	if migration != sqliteEngineMigration {
		t.Error("test migration not equals to actually used")
		return
	}

	db, err := sql.Open("sqlite", ":memory:")
	if err != nil {
		t.Error(err)
		return
	}

	err = db.Ping()
	if err != nil {
		t.Error(err)
		return
	}

	// создание структуры для работы с бд
	_, err = db.Exec(migration)
	if err != nil {
		t.Error(err)
		return
	}

	defer func() { _ = db.Close() }()

	tests := engineSqliteTestSuite{db}

	t.Run("save", tests.save)
	t.Run("close", tests.close)
	t.Run("export", tests.export)
	t.Run("construct", tests.construct)
}

type engineSqliteTestSuite struct{ db *sql.DB }

func (o engineSqliteTestSuite) getRandToken() string {
	b := make([]byte, 8)
	_, _ = rand.Read(b)
	t, _ := time.Now().MarshalBinary()
	return fmt.Sprintf("%x", md5.Sum(append(b, t...)))

}

func (o engineSqliteTestSuite) save(t *testing.T) {
	storage := NewStorageMock()

	t.Run("success", func(t *testing.T) {
		logsInput := []Log{
			{
				RequestId: uuid.New().String(),
				Namespace: "1.2.3",
				Level:     logDebug,
				Text:      o.getRandToken(),
				CreatedAt: time.Now(),
			},
			{
				RequestId: uuid.New().String(),
				Namespace: "1.2.3",
				Level:     logInfo,
				Text:      o.getRandToken(),
				CreatedAt: time.Now(),
			},
			{
				RequestId: uuid.New().String(),
				Namespace: "1.2.3",
				Level:     logError,
				Text:      o.getRandToken(),
				CreatedAt: time.Now(),
			},
		}

		engine := &EngineSqlite{
			db: o.db,
			//saveStmt: saveStmt,
		}

		err := engine.Save(storage, logsInput)
		assert.Nil(t, err)

		// check
		row, err := o.db.Query("SELECT namespace, level, request_id, text, created_at FROM logs")
		if err != nil {
			t.Errorf("can't check new logs was added: %v", err)
			return
		}

		defer func() { _ = row.Close() }()

		// ------- ------- ------- ------- -------

		logsStored := make([]Log, 0)
		for row.Next() {
			log := Log{}
			err = row.Scan(&log.Namespace, &log.Level, &log.RequestId, &log.Text, &log.CreatedAt)
			if err != nil {
				t.Error("can not scan")
				return
			}
			logsStored = append(logsStored, log)
		}

		assert.Equal(t, 3, len(logsStored))

	LoopSearch:
		for _, log := range logsInput {
			for _, l := range logsStored {
				if log.ToString() == l.ToString() {
					continue LoopSearch
				}
			}
			t.Errorf("log %s not found", log.ToString())
		}
	})

	t.Run("stmt fail", func(t *testing.T) {
		logsInput := []Log{
			{
				RequestId: uuid.New().String(),
				Namespace: "1.2.3",
				Level:     logDebug,
				Text:      o.getRandToken(),
				CreatedAt: time.Now(),
			},
			{
				RequestId: uuid.New().String(),
				Namespace: "1.2.3",
				//////////////////////////////////// invalid value
				Level:     "wtf",
				Text:      o.getRandToken(),
				CreatedAt: time.Now(),
			},
			{
				RequestId: uuid.New().String(),
				Namespace: "1.2.3",
				Level:     logError,
				Text:      o.getRandToken(),
				CreatedAt: time.Now(),
			},
		}

		engine := &EngineSqlite{
			db: o.db,
		}

		err := engine.Save(storage, logsInput)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "CHECK constraint failed")
	})

	t.Run("begin tx fail", func(t *testing.T) {
		db, err := sql.Open("sqlite", ":memory:")
		assert.Nil(t, err)

		engine := &EngineSqlite{
			db: db,
		}
		_ = db.Close()

		err = engine.Save(storage, nil)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "database is closed")
	})

	t.Run("begin prepare malformed table structure", func(t *testing.T) {
		t.Skip()
		db, err := sql.Open("sqlite", ":memory:")
		assert.Nil(t, err)
		defer func() { _ = db.Close() }()

		_, err = db.Exec("CREATE TABLE logs(xxx INTEGER );")
		assert.Nil(t, err)

		engine := &EngineSqlite{
			db: db,
		}

		err = engine.Save(storage, nil)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "table logs has no column named")
	})
}

func (o engineSqliteTestSuite) close(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	assert.Nil(t, err)
	defer func() { _ = db.Close() }()

	engine := &EngineSqlite{db}

	assert.Nil(t, engine.Close())
	assert.NotNil(t, db.Ping())
}

func (o engineSqliteTestSuite) export(t *testing.T) {

	storage := NewStorageMock()
	engine := &EngineSqlite{o.db}

	logs, err := engine.Pull(storage)
	assert.Nil(t, err)

	assert.Equal(t, 3, len(logs))

	// ------- ------- ------- ------- -------

	row, err := o.db.Query("SELECT namespace, level, request_id, text, created_at FROM logs WHERE is_exported is null")
	if err != nil {
		t.Errorf("can't check new logs was added: %v", err)
		return
	}

	defer func() { _ = row.Close() }()

	assert.False(t, row.Next())
}

func (o engineSqliteTestSuite) construct(t *testing.T) {

	storage := NewStorageMock()
	engine, err := NewSqliteEngine(":memory:")
	assert.Nil(t, err)

	logsInput := []Log{
		{
			Namespace: "1.2.3",
			Level:     logDebug,
			Text:      o.getRandToken(),
			CreatedAt: time.Now(),
		},
		{
			Namespace: "1.2.3",
			Level:     logInfo,
			Text:      o.getRandToken(),
			CreatedAt: time.Now(),
		},
		{
			Namespace: "1.2.3",
			Level:     logError,
			Text:      o.getRandToken(),
			CreatedAt: time.Now(),
		},
	}

	defer func() { _ = engine.Close() }()

	err = engine.Save(storage, logsInput)
	assert.Nil(t, err)
	logs, err := engine.Pull(storage)
	assert.Nil(t, err)
	assert.Equal(t, 3, len(logs))

	logs, err = engine.Pull(storage)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(logs))

}
