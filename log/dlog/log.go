package xlog

import (
	"fmt"
	"time"
)

type Log struct {
	RequestId string 	`json:"request_id"`
	Namespace string    `json:"namespace"`
	Level     string    `json:"level"`
	Text      string    `json:"text"`
	CreatedAt time.Time `json:"created_at"`
}

func (o Log) ToString() string {
	reqId := ""

	if o.RequestId != "" {
		reqId = o.RequestId + "."
	}

	return fmt.Sprintf("%s [%s] (%s%s) %s",
		o.CreatedAt.Format("2006-01-02 15:04:05"),
		o.Level, reqId, o.Namespace, o.Text)
}
