package xlog

import (
	"fmt"
	"strings"
	"time"
)

const (
	LevelError = LogLevel(iota)
	LevelInfo
	LevelDebug

	logError = "err"
	logInfo  = "inf"
	logDebug = "dbg"

	CtxKeyLogger ContextKey = "logger"
)

type (
	ContextKey string
	LogLevel   uint8
	Logger     interface {
		Error(data string)
		Info(data string)
		Debug(data string)
		WithPrefix(f string) Logger
		// @todo убрать нахер этот метод из интерфейса
		Close() error
	}
)

type XXLogger struct {
	requestId        string
	namespace        string
	storage          Storage
	errorMethod      func(e Log, s Storage)
	infoMethod       func(e Log, s Storage)
	debugMethod      func(e Log, s Storage)
	withPrefixMethod func(logger *XXLogger, str string) *XXLogger
}

func NewLogger(lvl LogLevel, s Storage) *XXLogger {
	logger := &XXLogger{
		namespace:        "x",
		storage:          s,
		errorMethod:      commonMethod,
		infoMethod:       commonMethod,
		debugMethod:      commonMethod,
		withPrefixMethod: WithPrefixDefault,
	}

	if lvl < LevelDebug {
		logger.debugMethod = voidMethod
	}

	if lvl < LevelInfo {
		logger.infoMethod = voidMethod
	}

	return logger
}

// ----------------------------------------------------------------------------

func (o *XXLogger) WithPrefix(f string) Logger {
	return o.withPrefixMethod(o, f)
}

func (o *XXLogger) Error(data string) {
	o.errorMethod(Log{
		RequestId: o.requestId,
		Namespace: o.namespace,
		Level:     logError,
		Text:      data,
		CreatedAt: time.Now(),
	}, o.storage)
}

func (o *XXLogger) Info(data string) {
	o.infoMethod(Log{
		RequestId: o.requestId,
		Namespace: o.namespace,
		Level:     logInfo,
		Text:      data,
		CreatedAt: time.Now(),
	}, o.storage)
}

func (o *XXLogger) Debug(data string) {
	o.debugMethod(Log{
		RequestId: o.requestId,
		Namespace: o.namespace,
		Level:     logDebug,
		Text:      data,
		CreatedAt: time.Now(),
	}, o.storage)
}

func (o *XXLogger) Close() error {
	o.errorMethod = voidMethod
	o.infoMethod = voidMethod
	o.debugMethod = voidMethod
	return o.storage.Close()
}

func commonMethod(log Log, s Storage) {
	go s.Put(log)
	fmt.Printf("%s\n", log.ToString())
}

func voidMethod(log Log, s Storage) {}

func WithPrefixDefault(logger *XXLogger, str string) *XXLogger {

	l := &XXLogger{
		requestId:        logger.requestId,
		namespace:        logger.namespace,
		storage:          logger.storage,
		errorMethod:      logger.errorMethod,
		infoMethod:       logger.infoMethod,
		debugMethod:      logger.debugMethod,
		withPrefixMethod: logger.withPrefixMethod,
	}

	if strings.HasPrefix(str, "-") {
		l.requestId = strings.TrimLeft(str, "-")
		return l
	}

	l.namespace = fmt.Sprintf("%s.%s", logger.namespace, str)
	return l
}
