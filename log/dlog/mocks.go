package xlog

func NewEngineMock() *CustomEngine {
	return &CustomEngine{
		closeFunc: func() error {
			return nil
		},
		saveFunc: func(storage Storage, logs []Log) error {
			return nil
		},
		pullFunc: func(storage Storage) ([]Log, error) {
			return nil, nil
		},
	}
}

func NewStorageMock() *CustomStorage {
	return &CustomStorage{
		engine:          NewEngineMock(),
		flushTicker:     nil,
		data:            make([]Log, 0),
		buffer:          make([]Log, 0),
		chanData:        make(chan Log),
		chanFlush:       make(chan bool),
		chanFlushResult: make(chan error),
		chanClose:       make(chan bool),
	}
}

func NewLoggerMock() *XXLogger {
	return &XXLogger{
		requestId:        "",
		namespace:        "mock",
		storage:          NewStorageMock(),
		errorMethod:      commonMethod,
		infoMethod:       commonMethod,
		debugMethod:      commonMethod,
		withPrefixMethod: WithPrefixDefault,
	}
}

type XXLoggerMock struct {
	Namespace      string
	ErrorFunc      func(data string)
	InfoFunc       func(data string)
	DebugFunc      func(data string)
	WithPrefixFunc func(logger *XXLoggerMock, str string) *XXLoggerMock
	CloseFunc      func() error
}

func (o *XXLoggerMock) Error(data string)          { o.ErrorFunc(data) }
func (o *XXLoggerMock) Info(data string)           { o.InfoFunc(data) }
func (o *XXLoggerMock) Debug(data string)          { o.DebugFunc(data) }
func (o *XXLoggerMock) WithPrefix(f string) Logger { return o.WithPrefixFunc(o, f) }
func (o *XXLoggerMock) Close() error               { return o.CloseFunc() }

func NewComplexLoggerMock() *XXLoggerMock {
	return &XXLoggerMock{
		Namespace:      "mock",
		ErrorFunc:      func(data string) {},
		InfoFunc:       func(data string) {},
		DebugFunc:      func(data string) {},
		WithPrefixFunc: WithPrefixMockFunc,
		CloseFunc:      func() error { return nil },
	}
}

func WithPrefixMockFunc(logger *XXLoggerMock, str string) *XXLoggerMock {
	return &XXLoggerMock{
		Namespace:      str,
		ErrorFunc:      logger.ErrorFunc,
		InfoFunc:       logger.InfoFunc,
		DebugFunc:      logger.DebugFunc,
		WithPrefixFunc: logger.WithPrefixFunc,
		CloseFunc:      logger.CloseFunc,
	}
}
