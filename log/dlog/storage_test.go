package xlog

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
	"time"
)

func TestStorageCustom(t *testing.T) {
	tests := customStorageTests{}
	t.Run("watch", tests.watch)
	t.Run("close", tests.close)
	t.Run("export", tests.export)
	t.Run("PutHandler", tests.onPut)
	t.Run("FlushHandler", tests.onFlush)
	t.Run("TickHandler", tests.onTicker)
	t.Run("PullHandler", tests.onPull)
	t.Run("CloseHandler", tests.onClose)
	t.Run("construct", tests.newCustomStorage)
}

type customStorageTests struct{}

func (o customStorageTests) watch(t *testing.T) {
	logExpected := Log{
		Namespace: "1.2.3",
		Level:     logInfo,
		Text:      "log text message",
		CreatedAt: time.Now(),
	}
	t.Run("PutHandler", func(t *testing.T) {
		storage := &CustomStorage{
			PutHandler: func(s *CustomStorage, log Log) {
				s.data = append(s.data, log)
				assert.Equal(t, logExpected.Text, log.Text)
			},
			CloseHandler: func(s *CustomStorage) error { return nil },

			data:            make([]Log, 0),
			chanData:        make(chan Log),
			chanClose:       make(chan bool),
			chanCloseResult: make(chan error),
		}

		go storage.Watch()

		// добавление двух записей
		storage.Put(logExpected)
		storage.Put(logExpected)

		assert.Nil(t, storage.Close())
		assert.Equal(t, 2, len(storage.data))
	})
	t.Run("FlushHandler", func(t *testing.T) {
		errorExpected := fmt.Errorf("[test] flush error")
		storage := &CustomStorage{
			FlushHandler: func(s *CustomStorage) error { return errorExpected },
			CloseHandler: func(s *CustomStorage) error { return nil },

			chanFlush:       make(chan bool),
			chanFlushResult: make(chan error),
			chanClose:       make(chan bool),
			chanCloseResult: make(chan error),
		}

		go storage.Watch()

		assert.Equal(t, errorExpected, storage.Flush())
		assert.Nil(t, storage.Close())
	})
	t.Run("TickHandler signal", func(t *testing.T) {
		// канал для блокировки теста
		subtestActionsChan := make(chan int)
		defer close(subtestActionsChan)

		m := sync.Mutex{}
		ticksCount := 0
		ticker := time.NewTicker(time.Millisecond * 50)

		storage := &CustomStorage{
			flushTicker: ticker,
			TickHandler: func(s *CustomStorage) {
				m.Lock()
				ticksCount++
				m.Unlock()
			},
		}

		go storage.Ticker()

		// отложенное закрытие канала тикера
		timer := time.AfterFunc(time.Millisecond*120, func() { ticker.Stop(); subtestActionsChan <- 1 })
		defer timer.Stop()

		<-subtestActionsChan
		m.Lock()
		assert.Equal(t, 2, ticksCount)
		m.Unlock()
	})
}

func (o customStorageTests) close(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		errorExpected := fmt.Errorf("[test] flush error")
		ticker := time.NewTicker(time.Hour)
		storage := &CustomStorage{
			flushTicker:  ticker,
			CloseHandler: func(s *CustomStorage) error { return errorExpected },

			chanData:        make(chan Log),
			chanFlush:       make(chan bool),
			chanFlushResult: make(chan error),
			chanClose:       make(chan bool),
			chanCloseResult: make(chan error),
		}

		go storage.Watch()

		assert.Equal(t, errorExpected, storage.Close())
		assert.NotNil(t, storage.Close())
	})
}

func (o customStorageTests) export(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		errorExpected := fmt.Errorf("[test] export error")
		storage := &CustomStorage{
			PullHandler: func(s *CustomStorage) ([]Log, error) {
				return nil, errorExpected
			},
			CloseHandler:    func(s *CustomStorage) error { return nil },
			chanClose:       make(chan bool),
			chanCloseResult: make(chan error),
		}

		go storage.Watch()

		logs, err := storage.Pull()

		assert.Nil(t, logs)
		assert.Equal(t, errorExpected, err)
		assert.Nil(t, storage.Close())
	})
}

func (o customStorageTests) onPut(t *testing.T) {
	logExpected := Log{
		Namespace: "1.2.3",
		Level:     logInfo,
		Text:      "log text message",
		CreatedAt: time.Now(),
	}
	t.Run("success", func(t *testing.T) {
		storage := &CustomStorage{
			data: make([]Log, 0),
		}
		DefaultPutHandler(storage, logExpected)
		assert.Equal(t, 1, len(storage.data))
	})
}

func (o customStorageTests) onFlush(t *testing.T) {
	logExpected := Log{
		Namespace: "1.2.3",
		Level:     logInfo,
		Text:      "log text message",
		CreatedAt: time.Now(),
	}

	t.Run("success", func(t *testing.T) {
		storage := &CustomStorage{
			engine: &CustomEngine{
				saveFunc: func(storage Storage, logs []Log) error {
					assert.Equal(t, logExpected.Text, logs[0].Text)
					return nil
				},
			},
			data:   make([]Log, 0),
			buffer: make([]Log, 0),
		}

		storage.data = append(storage.data, logExpected)
		assert.Nil(t, DefaultFlushHandler(storage))
		assert.Equal(t, 0, len(storage.data))
		assert.Equal(t, 0, len(storage.buffer))
	})
	t.Run("storage fail", func(t *testing.T) {
		errorExpected := fmt.Errorf("[test] save error")
		storage := &CustomStorage{
			engine: &CustomEngine{
				saveFunc: func(storage Storage, logs []Log) error {
					assert.Equal(t, logExpected.Text, logs[0].Text)
					return errorExpected
				},
			},
			data:   make([]Log, 0),
			buffer: make([]Log, 0),
		}

		storage.data = append(storage.data, logExpected)
		assert.Equal(t, errorExpected, DefaultFlushHandler(storage))
		assert.Equal(t, 0, len(storage.data))
		assert.Equal(t, 1, len(storage.buffer))
	})
}

func (o customStorageTests) onTicker(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		flushTriggered := 0

		storage := &CustomStorage{
			FlushHandler: func(s *CustomStorage) error {
				flushTriggered++
				return nil
			},
			CloseHandler: func(s *CustomStorage) error {
				close(s.chanFlush)
				close(s.chanFlushResult)
				close(s.chanClose)
				return nil
			},
			chanFlush:       make(chan bool),
			chanFlushResult: make(chan error),
			chanClose:       make(chan bool),
			chanCloseResult: make(chan error),
		}

		go storage.Watch()
		defer func() {_ = storage.Close()}()

		DefaultTickHandler(storage)
		assert.Equal(t, 1, flushTriggered)
	})
	t.Run("flush fail", func(t *testing.T) {
		errorExpected := fmt.Errorf("[test] flush error")
		flushTriggered := 0
		storage := &CustomStorage{
			FlushHandler: func(s *CustomStorage) error {
				flushTriggered++
				return errorExpected
			},
			CloseHandler: func(s *CustomStorage) error {
				close(s.chanFlush)
				close(s.chanFlushResult)
				close(s.chanClose)
				return nil
			},
			chanFlush:       make(chan bool),
			chanFlushResult: make(chan error),
			chanClose:       make(chan bool),
			chanCloseResult: make(chan error),
		}

		go storage.Watch()
		defer func() {_ = storage.Close()}()

		DefaultTickHandler(storage)
		assert.Equal(t, 1, flushTriggered)
	})
}

func (o customStorageTests) onPull(t *testing.T) {
	count := 0
	t.Run("success", func(t *testing.T) {
		storage := &CustomStorage{
			engine: &CustomEngine{
				pullFunc: func(storage Storage) ([]Log, error) {
					count++
					return []Log{{}}, nil
				},
			},
			PullHandler: DefaultPullHandler,
		}
		logs, err := DefaultPullHandler(storage)
		assert.Nil(t, err)
		assert.Equal(t, 1, count)
		assert.Equal(t, 1, len(logs))
	})
}

func (o customStorageTests) onClose(t *testing.T) {
	count := 0
	t.Run("success", func(t *testing.T) {
		errorExpected := fmt.Errorf("[test] flush error")
		ticker := time.NewTicker(time.Millisecond * 50)

		engine := NewEngineMock()
		engine.closeFunc = func() error {
			return fmt.Errorf("[test] storage close error")
		}

		storage := &CustomStorage{
			engine: engine,
			flushTicker:     ticker,
			FlushHandler:    func(s *CustomStorage) error { count++; return errorExpected },
			chanData:        make(chan Log),
			chanClose:       make(chan bool),
			chanCloseResult: make(chan error),
			chanFlush:       make(chan bool),
			chanFlushResult: make(chan error),
		}

		err := DefaultCloseHandler(storage)
		assert.Equal(t, errorExpected, err)
		assert.Equal(t, 1, count)
	})
}

func (o customStorageTests) newCustomStorage(t *testing.T) {
	isFlushed := false
	t.Run("success", func(t *testing.T) {

		storage := NewCustomStorage(NewEngineMock(), time.Hour)
		storage.FlushHandler = func(s *CustomStorage) error {
			isFlushed = true
			return DefaultFlushHandler(s)
		}

		assert.Nil(t, storage.Close())
		assert.True(t, isFlushed)
	})
}