package xlog

type Engine interface {
	Close() error
	Save(Storage, []Log) error
	Pull(Storage) ([]Log, error)
}

// ----------------------------------------------------------------------------

type CustomEngine struct {
	closeFunc func() error
	saveFunc  func(Storage, []Log) error
	pullFunc  func(Storage) ([]Log, error)
}

func (o *CustomEngine) Close() error                     { return o.closeFunc() }
func (o *CustomEngine) Save(s Storage, logs []Log) error { return o.saveFunc(s, logs) }
func (o *CustomEngine) Pull(s Storage) ([]Log, error)    { return o.pullFunc(s) }
