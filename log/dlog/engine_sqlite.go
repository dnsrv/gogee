package xlog

import (
	"database/sql"
	"fmt"

	_ "modernc.org/sqlite"
)

var (
	sqliteEngineMigration = `
		CREATE TABLE IF NOT EXISTS logs (
			id 			INTEGER PRIMARY KEY AUTOINCREMENT, 
			namespace	VARCHAR(128) NULL,
			level       CHAR(3) NOT NULL CHECK (level in ('inf', 'dbg', 'err')),
			request_id	CHAR(36) NULL,
			text 	    VARCHAR(256) NOT NULL,
			is_exported INT NULL,
			created_at 	DATETIME     NULL DEFAULT CURRENT_TIMESTAMP
		);
	`
)

type EngineSqlite struct {
	db *sql.DB
}

func NewSqliteEngine(dbFilepath string) (*EngineSqlite, error) {
	conn, err := sql.Open("sqlite", dbFilepath)
	if err != nil {
		return nil, fmt.Errorf("[logger][sqlite][connect] can't open file: %v", err)
	}

	if err = conn.Ping(); err != nil {
		return nil, fmt.Errorf("[logger][sqlite][connect] ping error: %v", err)
	}

	_, err = conn.Exec(sqliteEngineMigration)
	if err != nil {
		return nil, fmt.Errorf("[logger][sqlite][migration] can not create logs table: %v", err)
	}

	return &EngineSqlite{
		db: conn,
	}, nil
}

// ----------------------------------------------------------------------------

func (o *EngineSqlite) Close() error {
	return o.db.Close()
}

func (o *EngineSqlite) Save(s Storage, logs []Log) error {
	tx, err := o.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("INSERT INTO logs (namespace, level, request_id, text, created_at) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		return err
	}

	defer func() { _ = tx.Commit() }()

	for _, log := range logs {
		_, errRes := stmt.Exec(log.Namespace, log.Level, log.RequestId, log.Text, log.CreatedAt)
		if errRes != nil {
			_ = tx.Rollback()
			return errRes
		}
	}

	return nil
}

func (o *EngineSqlite) Pull(s Storage) ([]Log, error) {

	row, err := o.db.Query("SELECT namespace, level, request_id, text, created_at FROM logs WHERE is_exported is null")
	if err != nil {
		return nil, err
	}

	defer func() { _ = row.Close() }()

	logs := make([]Log, 0)

	for row.Next() {
		log := Log{}
		err = row.Scan(&log.Namespace, &log.Level, &log.RequestId, &log.Text, &log.CreatedAt)
		if err != nil {
			return nil, err
		}
		logs = append(logs, log)
	}

	_, err = o.db.Exec("UPDATE logs SET is_exported = 1 WHERE is_exported is null")
	return logs, err
}
