package xlog

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestLog(t *testing.T) {
	log := Log{
		RequestId: "reqId",
		Namespace: "1.2.3",
		Level:     logError,
		Text:      "texts",
		CreatedAt: time.Now(),
	}

	str := fmt.Sprintf("%s [%s] (reqId.%s) %s",
		log.CreatedAt.Format("2006-01-02 15:04:05"),
		log.Level, log.Namespace, log.Text)
	assert.Equal(t, str, log.ToString())
}
