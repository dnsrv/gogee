package xlog

import (
	"fmt"
	"time"
)

type Storage interface {
	Put(Log)
	Flush() error
	Pull() ([]Log, error)
	Close() error
}

type CustomStorage struct {
	isClosed    bool
	engine      Engine
	flushTicker *time.Ticker

	PutHandler   func(s *CustomStorage, log Log)
	FlushHandler func(s *CustomStorage) error
	TickHandler  func(s *CustomStorage)
	CloseHandler func(s *CustomStorage) error
	PullHandler  func(s *CustomStorage) ([]Log, error)

	data            []Log
	buffer          []Log
	chanData        chan Log
	chanFlush       chan bool
	chanFlushResult chan error
	chanClose       chan bool
	chanCloseResult chan error
}

func NewCustomStorage(engine Engine, flushInterval time.Duration) *CustomStorage {
	storage := &CustomStorage{
		engine:      engine,
		flushTicker: time.NewTicker(flushInterval),

		data:            make([]Log, 0),
		buffer:          make([]Log, 0),
		chanData:        make(chan Log),
		chanFlush:       make(chan bool),
		chanFlushResult: make(chan error),
		chanClose:       make(chan bool),
		chanCloseResult: make(chan error),

		PutHandler:   DefaultPutHandler,
		FlushHandler: DefaultFlushHandler,
		TickHandler:  DefaultTickHandler,
		CloseHandler: DefaultCloseHandler,
		PullHandler:  DefaultPullHandler,
	}

	go storage.Watch()
	go storage.Ticker()

	return storage
}

func (o *CustomStorage) Watch() {
	for {
		select {
		case log := <-o.chanData:
			o.PutHandler(o, log)
		case <-o.chanFlush:
			o.chanFlushResult <- o.FlushHandler(o)
		case <-o.chanClose:
			o.chanCloseResult <- o.CloseHandler(o)
			return
		}
	}
}

func (o *CustomStorage) Ticker() {
	for {
		select {
		case <-o.flushTicker.C:
			o.TickHandler(o)
		}
	}
}

func (o *CustomStorage) Put(e Log) {
	o.chanData <- e
}

func (o *CustomStorage) Flush() error {
	o.chanFlush <- true
	return <-o.chanFlushResult
}

func (o *CustomStorage) Pull() ([]Log, error) {
	return o.PullHandler(o)
}

func (o *CustomStorage) Close() error {
	if o.isClosed {
		return fmt.Errorf("[logger][close] storage already closed")
	}

	o.isClosed = true
	o.chanClose <- true

	closeResult := <-o.chanCloseResult
	close(o.chanCloseResult)
	return closeResult
}

// ----------------------------------------------------------------------------

func DefaultPutHandler(s *CustomStorage, log Log) {
	s.data = append(s.data, log)
}

func DefaultFlushHandler(s *CustomStorage) error {
	fmt.Printf("[logger][flush] flushing\n")
	s.buffer = append(s.buffer, s.data[0:]...)
	s.data = s.data[:0]

	fmt.Printf("[logger][flush] savind buffer\n")
	err := s.engine.Save(s, s.buffer)
	if err != nil {
		return err
	}

	fmt.Printf("[logger][flush] success\n")
	s.buffer = s.buffer[:0]

	return nil
}

func DefaultTickHandler(s *CustomStorage) {
	fmt.Printf("[logger][ticker] flushing\n")
	err := s.Flush()
	if err != nil {
		fmt.Printf("[logger][ticker] flush error: %v\n", err)
	}
}

func DefaultPullHandler(s *CustomStorage) ([]Log, error) {
	fmt.Printf("[logger][pull] pulling logs\n")
	return s.engine.Pull(s)
}

func DefaultCloseHandler(s *CustomStorage) error {
	s.flushTicker.Stop()
	defer func() {
		close(s.chanData)
		close(s.chanClose)
		close(s.chanFlush)
		close(s.chanFlushResult)
		fmt.Printf("[logger][stogare] closed\n")
	}()

	res := s.FlushHandler(s)

	err := s.engine.Close()
	if err != nil {
		fmt.Printf("[logger][close] engine error: %v\n", err)
	}

	return res
}
