package xlog

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
	"time"
)

func TestLogger(t *testing.T) {
	tc := loggerTestSuite{}
	t.Run("methods", tc.methods)
	t.Run("close", tc.close)
	t.Run("withPrefix", tc.withPrefix)
	t.Run("construct", tc.construct)
}

type loggerTestSuite struct {}

func (o loggerTestSuite) methods(t *testing.T) {
	text := "test log text"

	storage := NewStorageMock()
	storage.PutHandler = func(s *CustomStorage, log Log) {
		assert.Equal(t, text, log.Text)
	}

	assertionFunc := func(e Log, s Storage) {
		assert.Equal(t, text, e.Text)
	}

	logger := &XXLogger{
		namespace:   "x",
		storage:     storage,
		errorMethod: assertionFunc,
		infoMethod: assertionFunc,
		debugMethod: assertionFunc,
	}

	logger.Debug(text)
	logger.Info(text)
	logger.Error(text)

	commonMethod(Log{Text: text}, storage)
}

func (o loggerTestSuite) close(t *testing.T) {
		storage := NewCustomStorage(NewEngineMock(), time.Minute)

	logger := &XXLogger{
		storage:     storage,
	}

	assert.Nil(t, logger.Close())
	assert.True(t, storage.isClosed)
}

func (o loggerTestSuite) withPrefix(t *testing.T) {

	t.Run("after", func(t *testing.T) {
		logger := &XXLogger{
			namespace: "x",
			storage:     NewStorageMock(),
			withPrefixMethod: WithPrefixDefault,
			infoMethod: func(e Log, s Storage) {
				assert.Equal(t, "x.y.z", e.Namespace)
			},
		}

		l2 := logger.WithPrefix("y.z")
		l2.Info("test log text")
	})
	t.Run("before", func(t *testing.T) {
		logger := &XXLogger{
			namespace: "x",
			storage:     NewStorageMock(),
			withPrefixMethod: WithPrefixDefault,
			infoMethod: func(e Log, s Storage) {
				assert.Equal(t, "x", e.Namespace)
			},
		}

		l2 := logger.WithPrefix("-z")
		l2.Info("test log text")
	})
}

func (o loggerTestSuite) construct(t *testing.T) {
	s := NewStorageMock()
	logger := NewLogger(LevelError, s)

	assert.Equal(t,
		reflect.ValueOf(logger.debugMethod).Pointer(),
		reflect.ValueOf(voidMethod).Pointer(),
		)

	assert.Equal(t,
		reflect.ValueOf(logger.infoMethod).Pointer(),
		reflect.ValueOf(voidMethod).Pointer(),
		)

	assert.NotEqual(t,
		reflect.ValueOf(logger.errorMethod).Pointer(),
		reflect.ValueOf(voidMethod).Pointer(),
		)

}


