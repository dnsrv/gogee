package xlog

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewEngineMock(t *testing.T) {
	mock := NewEngineMock()

	assert.Nil(t, mock.Close())
	assert.Nil(t, mock.Save(nil, nil))

	logs, err := mock.Pull(nil)
	assert.Nil(t, logs)
	assert.Nil(t, err)
}

func TestNewStorageMock(t *testing.T) {
	l := NewLoggerMock()
	l.Info("test")
}