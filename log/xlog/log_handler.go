package xlog

import (
	"time"
)

type LogStatus int
type LogLevel int

const (
	LOG_DEBUG   = LogStatus(0)
	LOG_INFO    = LogStatus(1)
	LOG_WARNING = LogStatus(2)
	LOG_ERROR   = LogStatus(3)

	LOG_LEVEL_ERROR   = LogLevel(3)
	LOG_LEVEL_WARNING = LogLevel(2)
	LOG_LEVEL_INFO    = LogLevel(1)
	LOG_LEVEL_DEBUG   = LogLevel(0)
)

var (
	DateTimeFormat = "2006-01-02 15:04:05"
	LogBuffer      = 100
)

type ILogHandler interface {
	Debug(code string, msg string)
	Info(code string, msg string)
	Warn(code string, msg string)
	Error(code string, msg string)
	GetLevel() int
}

type ILog interface {
	Status() int
	Code() string
	Message() string
	ToString() string
}

type Log struct {
	status    int
	code      string
	message   string
	createdAt time.Time
}

func (o Log) Status() int {
	return o.status
}

func (o Log) Code() string {
	return o.code
}

func (o Log) Message() string {
	return o.message
}

func (o Log) ToString() string {
	var level string
	switch LogStatus(o.status) {
	case LOG_DEBUG:
		level = "DBG"
	case LOG_INFO:
		level = "INF"
	case LOG_WARNING:
		level = "WRN"
	case LOG_ERROR:
		level = "ERR"
	default:
		level = "ERR"
	}

	return o.createdAt.Format(DateTimeFormat) + " [" + level + "] (" + o.code + ") " + o.message
}

// ----------------------------------------------------------------------------

type LogHandler struct {
	level      LogLevel
	dbgHandler func(code string, msg string)
	infHandler func(code string, msg string)
	wrnHandler func(code string, msg string)
	errHandler func(code string, msg string)
	logs       chan ILog
	handler    func(l ILog)
}

func NewLogHandler(handlerFunc func(l ILog), level LogLevel) *LogHandler {
	logChan := make(chan ILog, LogBuffer)
	go func() {
		for l := range logChan {
			handlerFunc(l)
		}
	}()

	errHandler := func(code string, msg string) {
		logChan <- Log{status: int(LOG_ERROR), code: code, message: msg, createdAt: time.Now()}
	}
	wrnHandler := func(code string, msg string) {
		logChan <- Log{status: int(LOG_WARNING), code: code, message: msg, createdAt: time.Now()}
	}
	infHandler := func(code string, msg string) {
		logChan <- Log{status: int(LOG_INFO), code: code, message: msg, createdAt: time.Now()}
	}
	dbgHandler := func(code string, msg string) {
		logChan <- Log{status: int(LOG_DEBUG), code: code, message: msg, createdAt: time.Now()}
	}

	if level > LOG_LEVEL_WARNING {
		wrnHandler = func(code string, msg string) {}
	}
	if level > LOG_LEVEL_INFO {
		infHandler = func(code string, msg string) {}
	}
	if level > LOG_LEVEL_DEBUG {
		dbgHandler = func(code string, msg string) {}
	}

	return &LogHandler{
		logs:       logChan,
		level:      level,
		handler:    handlerFunc,
		dbgHandler: dbgHandler,
		infHandler: infHandler,
		wrnHandler: wrnHandler,
		errHandler: errHandler,
	}
}

func (o *LogHandler) Debug(code string, msg string) { o.dbgHandler(code, msg) }
func (o *LogHandler) Info(code string, msg string)  { o.infHandler(code, msg) }
func (o *LogHandler) Warn(code string, msg string)  { o.wrnHandler(code, msg) }
func (o *LogHandler) Error(code string, msg string) { o.errHandler(code, msg) }
func (o *LogHandler) GetLevel() int                 { return int(o.level) }

// ----------------------------------------------------------------------------

type LogHandlerMock struct {
	DbgFunc      func(code string, msg string)
	InfoFunc     func(code string, msg string)
	WarnFunc     func(code string, msg string)
	ErrorFunc    func(code string, msg string)
	GetLevelFunc func() int
}

func NewLogHandlerMock(lvl int) *LogHandlerMock {
	return &LogHandlerMock{
		DbgFunc:      func(code string, msg string) {},
		InfoFunc:     func(code string, msg string) {},
		WarnFunc:     func(code string, msg string) {},
		ErrorFunc:    func(code string, msg string) {},
		GetLevelFunc: func() int { return lvl },
	}
}

func (o *LogHandlerMock) Debug(code string, msg string) { o.DbgFunc(code, msg) }
func (o *LogHandlerMock) Info(code string, msg string)  { o.InfoFunc(code, msg) }
func (o *LogHandlerMock) Warn(code string, msg string)  { o.WarnFunc(code, msg) }
func (o *LogHandlerMock) Error(code string, msg string) { o.ErrorFunc(code, msg) }
func (o *LogHandlerMock) GetLevel() int                 { return o.GetLevelFunc() }
