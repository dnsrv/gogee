package xlog

import (
	"testing"
)

func BenchmarkLogHandler(b *testing.B) {
	logger := NewLogHandler(func(l ILog) {}, LOG_LEVEL_DEBUG)

	for i := 0; i < b.N; i++ {
		logger.Debug("code", "message")
	}
}
