package jwt

import (
	"fmt"
	"net/http"
	"testing"
	"time"

	jwt4 "github.com/dgrijalva/jwt-go/v4"
	"github.com/stretchr/testify/assert"
)

func TestTokenizer(t *testing.T) {
	testSuite := tokenizerTestSuite{
		Signature:   "signature",
		Expiration:  time.Minute,
		KeyFunction: KeyFunctionHmac,
		User: User{
			ID:          "4917",
			Username:    "test",
			Email:       "dnsrv@mail.ru",
			Permissions: []string{"p1", "p2"},
		},
	}
	t.Run("createToken", testSuite.createToken)
	t.Run("parseHeader", testSuite.parseHeader)
	t.Run("parseToken", testSuite.parseToken)
	t.Run("keyFunction HMAC", testSuite.keyFunctionHMAC)
}

type tokenizerTestSuite struct {
	User        User
	Signature   string
	Expiration  time.Duration
	KeyFunction func(string) KeyFunction
}

func (o tokenizerTestSuite) createToken(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		tokenizer := NewTokenizer(o.Signature, o.KeyFunction)
		token, err := tokenizer.CreateToken(o.User, time.Duration(100*365*24*time.Hour))
		assert.Nil(t, err)
		assert.NotEmpty(t, token)
		//t.Error("token:", token)
	})
}

func (o tokenizerTestSuite) parseHeader(t *testing.T) {
	tokenizer := NewTokenizer(o.Signature, o.KeyFunction)
	t.Run("success", func(t *testing.T) {
		header := http.Header{}
		header.Set(tokenizer.HeaderAuthKey, fmt.Sprintf("%s %s", tokenizer.HeaderAuthMethod, "1331"))
		tokenString, err := tokenizer.ParseHeader(header)

		assert.Nil(t, err)
		assert.Equal(t, tokenString, "1331")
	})
	t.Run("no header", func(t *testing.T) {
		header := http.Header{}
		tokenString, err := tokenizer.ParseHeader(header)

		assert.Equal(t, ErrAuthHeaderNotFound, err)
		assert.Equal(t, tokenString, "")
	})
	t.Run("invalid header format", func(t *testing.T) {
		header := http.Header{}
		header.Set(tokenizer.HeaderAuthKey, fmt.Sprintf("%s%s", tokenizer.HeaderAuthMethod, "1331"))
		tokenString, err := tokenizer.ParseHeader(header)

		assert.Equal(t, ErrAuthInvalidHeaderFormat, err)
		assert.Equal(t, tokenString, "")
	})
	t.Run("invalid header val type (not Bearer)", func(t *testing.T) {
		header := http.Header{}
		header.Set(tokenizer.HeaderAuthKey, fmt.Sprintf("%s %s", "NotBearer", "1331"))
		tokenString, err := tokenizer.ParseHeader(header)

		assert.Equal(t, ErrAuthInvalidMethod, err)
		assert.Equal(t, tokenString, "")
	})
}

func (o tokenizerTestSuite) parseToken(t *testing.T) {
	var testValidToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjQ4NDY3NjY5NDQuMjc4MzU2LCJpYXQiOjE2OTMxNjY5NDQuMjc4MzcwMSwidXNlciI6eyJpZCI6IjQ5MTciLCJ1c2VybmFtZSI6InRlc3QiLCJlbWFpbCI6ImRuc3J2QG1haWwucnUiLCJwZXJtaXNzaW9ucyI6WyJwMSIsInAyIl19fQ.x-PdL8K2RKxvrQNNUyhb8VjHTjb3Llk69rMRIlY64ZA"
	t.Run("success", func(t *testing.T) {
		tokenizer := NewTokenizer(o.Signature, o.KeyFunction)
		claims, err := tokenizer.ParseToken(testValidToken)

		assert.Nil(t, err)
		assert.Equal(t, o.User.ID, claims.User.ID)
		assert.Equal(t, o.User.Username, claims.User.Username)
		assert.Equal(t, o.User.Email, claims.User.Email)
		assert.Equal(t, o.User.Permissions, claims.User.Permissions)
	})
	t.Run("invalid signature", func(t *testing.T) {
		invalidFunc := func(secret string) KeyFunction {
			return func(token *jwt4.Token) (interface{}, error) {
				return []byte("invalid_signature"), nil
			}
		}

		tokenizer := NewTokenizer("invalid_signature", invalidFunc)
		claims, err := tokenizer.ParseToken(testValidToken)

		assert.NotNil(t, err)
		assert.Nil(t, claims)
	})
	t.Run("invalid token", func(t *testing.T) {
		tokenizer := NewTokenizer(o.Signature, o.KeyFunction)
		claims, err := tokenizer.ParseToken("invalid token")

		assert.NotNil(t, err)
		assert.Nil(t, claims)
	})
	t.Run("invalid algorithm", func(t *testing.T) {
		kf := func(secret string) KeyFunction {
			return func(token *jwt4.Token) (interface{}, error) {
				return nil, ErrAuthInvalidAlg
			}
		}
		tokenizer := NewTokenizer(o.Signature, kf)
		claims, err := tokenizer.ParseToken(testValidToken)

		assert.NotNil(t, err)
		assert.Nil(t, claims)
	})
	t.Run("expired token", func(t *testing.T) {
		expiredToken := "eyJhbGciOiJIUzM4NCIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2Mzk1MTMxMDQuMDA5NDQyLCJpYXQiOjE2Mzk1MDk1MDQuMDA5NDQyLCJwYXlsb2FkIjp7Im5vZGVfaWQiOiI0OTE3In19._DnmHJsRpnJDyOj1wH06lmqW-oTmjo37obYHiepzqFn8n_lf6xVq1edeApu1980i"
		tokenizer := NewTokenizer(o.Signature, o.KeyFunction)
		claims, err := tokenizer.ParseToken(expiredToken)

		assert.NotNil(t, err)
		assert.Nil(t, claims)
	})
}

func (o tokenizerTestSuite) keyFunctionHMAC(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		signingKey := "signature"
		token := jwt4.New(jwt4.SigningMethodHS256)
		keyFunc := KeyFunctionHmac(signingKey)

		sign, err := keyFunc(token)
		assert.Nil(t, err)
		assert.Equal(t, []byte(signingKey), sign)
	})
	t.Run("invalid algorithm", func(t *testing.T) {
		signingKey := "signature"
		token := jwt4.New(jwt4.SigningMethodES384)
		keyFunc := KeyFunctionHmac(signingKey)

		sign, err := keyFunc(token)
		assert.Equal(t, ErrAuthInvalidAlg, err)
		assert.Equal(t, nil, sign)
	})
}
