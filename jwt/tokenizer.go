package jwt

import (
	"net/http"
	"strings"
	"time"

	jwt4 "github.com/dgrijalva/jwt-go/v4"
)

const (
	DefaultHeaderKey        = "Authorization"
	DefaultHeaderAuthMethod = "Bearer"
	HeaderSeparator         = " "
)

type KeyFunction func(token *jwt4.Token) (interface{}, error)

type Tokenizer struct {
	signingKey       []byte
	keyFunction      func(token *jwt4.Token) (interface{}, error)
	HeaderAuthKey    string
	HeaderAuthMethod string
}

func NewTokenizer(signingKey string, keyFunction func(string) KeyFunction) *Tokenizer {
	return &Tokenizer{
		signingKey:       []byte(signingKey),
		keyFunction:      keyFunction(signingKey),
		HeaderAuthKey:    DefaultHeaderKey,
		HeaderAuthMethod: DefaultHeaderAuthMethod,
	}
}

func (o *Tokenizer) CreateToken(u User, ttl time.Duration) (string, error) {
	return jwt4.NewWithClaims(jwt4.SigningMethodHS256, &Claims{
		StandardClaims: jwt4.StandardClaims{
			ExpiresAt: jwt4.At(time.Now().Add(ttl)),
			IssuedAt:  jwt4.At(time.Now()),
		},
		User: u,
	}).SignedString(o.signingKey)
}

func (o *Tokenizer) ParseHeader(header http.Header) (string, error) {
	authHeader := header.Get(o.HeaderAuthKey)
	if authHeader == "" {
		return "", ErrAuthHeaderNotFound
	}

	headerParts := strings.Split(authHeader, HeaderSeparator)
	if len(headerParts) != 2 {
		return "", ErrAuthInvalidHeaderFormat
	}

	if headerParts[0] != o.HeaderAuthMethod {
		return "", ErrAuthInvalidMethod
	}

	return headerParts[1], nil
}

func (o *Tokenizer) ParseToken(accessToken string) (*Claims, error) {
	token, err := jwt4.ParseWithClaims(accessToken, &Claims{}, o.keyFunction)
	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(*Claims); ok && token.Valid {
		return claims, nil
	}

	return nil, ErrAuthInvalidToken
}

func KeyFunctionHmac(signingKey string) KeyFunction {
	return func(token *jwt4.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt4.SigningMethodHMAC); !ok {
			return nil, ErrAuthInvalidAlg
		}
		return []byte(signingKey), nil
	}
}
