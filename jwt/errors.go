package jwt

import "errors"

var (
	ErrAuthRequired            = errors.New("authorization required")
	ErrPermissionDenied        = errors.New("permission denied")
	ErrAuthHeaderNotFound      = errors.New("authorization header not found")
	ErrAuthInvalidHeaderFormat = errors.New("invalid auth header value format")
	ErrAuthInvalidMethod       = errors.New("invalid auth header type")
	ErrAuthInvalidAlg          = errors.New("unexpected signing method")
	ErrAuthInvalidToken        = errors.New("invalid auth token")
	ErrUserDataNotFound        = errors.New("user data not found")
)
