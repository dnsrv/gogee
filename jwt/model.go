package jwt

import jwt4 "github.com/dgrijalva/jwt-go/v4"

var (
	IsRootPermission = func(p string) bool {
		return p == "root"
	}
)

type User struct {
	ID          string   `json:"id"`
	Username    string   `json:"username"`
	Email       string   `json:"email"`
	Permissions []string `json:"permissions"`
}

func (o *User) HasPermission(permission string) bool {
	for _, v := range o.Permissions {
		if v == permission || IsRootPermission(v) {
			return true
		}
	}
	return false
}

type Claims struct {
	jwt4.StandardClaims
	User User `json:"user"`
}
