package jwt

import "net/http"

type TokenizerMock struct {
	CreateTokenFunc func(payload map[string]string) (string, error)
	ParseHeaderFunc func(header http.Header) (string, error)
	ParseTokenFunc func(accessToken string) (*Claims, error)
}

func (o *TokenizerMock) CreateToken(payload map[string]string) (string, error) {
	return o.CreateTokenFunc(payload)
}
func (o *TokenizerMock) ParseHeader(header http.Header) (string, error) {
	return o.ParseHeaderFunc(header)
}
func (o *TokenizerMock) ParseToken(accessToken string) (*Claims, error) {
	return o.ParseTokenFunc(accessToken)
}