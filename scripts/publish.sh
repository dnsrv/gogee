#!/bin/sh

clear

# getting the current directory
START_DIR=$(pwd)

# up one level
cd ..

# loading dependencies
echo "go get -v ../..."
clear

# getting the latest version of the package (git tag)
CURRENT_VERSION=$(git describe --tags --abbrev=0)

# printing the current version
echo "Current version: $CURRENT_VERSION"

# if new version is provided in args, then we will use it
if [ -z "$1" ]; then
    echo "Please provide a new version: "
    read NEW_VERSION
else
    echo "New version provided: $1"
    NEW_VERSION="$1"
fi

# if old version is equal to new version, then we will exit
if [ "$CURRENT_VERSION" = "$NEW_VERSION" ]; then
    echo "Old version is equal to new version. Exiting ..."
    cd "$START_DIR"
    exit 0
fi

# printing the new version
echo "New version: $NEW_VERSION"

# enter commit message
echo "Enter commit message: "
read COMMIT_MESSAGE

if [ -z "$COMMIT_MESSAGE" ]; then
    echo "Commit message is empty. Exiting ..."
    cd "$START_DIR"
    exit 0
fi

git add --all
git commit -m "$COMMIT_MESSAGE"
git tag "v$NEW_VERSION"
git push --tags origin dev

# hint for the user
echo "======================================================================="
echo "== ! IMPORTANT ! --- first publish tag to master in repo !!! =========="
echo "======================================================================="

echo "use go get gitlab.com/dnsrv/gogee@v$NEW_VERSION to get the latest version of the package"

cd "$START_DIR"

exit 0
