package dcfg

import (
	"os"
	"strconv"
	"strings"
	"time"
)

type Envator struct {
}

// GetEnvAsString Simple helper function to read an environment or return a default value
func (o *Envator) GetEnvAsString(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return defaultVal
}

// GetEnvAsInt Simple helper function to read an environment variable into integer or return a default value
func (o *Envator) GetEnvAsInt(name string, defaultVal int) int {
	valueStr := o.GetEnvAsString(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}
	return defaultVal
}

// GetEnvAsBool Helper to read an environment variable into a bool or return default value
func (o *Envator) GetEnvAsBool(name string, defaultVal bool) bool {
	valStr := o.GetEnvAsString(name, "")
	if val, err := strconv.ParseBool(valStr); err == nil {
		return val
	}
	return defaultVal
}

// GetEnvAsSlice Helper to read an environment variable into a string slice or return default value
func (o *Envator) GetEnvAsSlice(name string, defaultVal []string, sep string) []string {
	valStr := o.GetEnvAsString(name, "")
	if valStr == "" {
		return defaultVal
	}
	val := strings.Split(valStr, sep)
	return val
}

// GetEnvAsIntDuration Helper to read an environment variable into a string slice or return default value
func (o *Envator) GetEnvAsIntDuration(name string, defaultVal int, t time.Duration) time.Duration {
	valInt := o.GetEnvAsInt(name, 0)
	if valInt == 0 {
		return t * time.Duration(defaultVal)
	}

	return t * time.Duration(valInt)
}

// GetEnvAsDuration Helper to read an environment variable into a string slice or return default value
func (o *Envator) GetEnvAsDuration(name string, defaultVal int, t time.Duration) time.Duration {
	val := o.GetEnvAsString(name, "")
	if val == "" {
		return t * time.Duration(defaultVal)
	}

	d, err := time.ParseDuration(val)
	if err != nil {
		return t * time.Duration(defaultVal)
	}

	return d
}
