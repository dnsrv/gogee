package dcfg

import (
	"math"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type setTimezoneOffsetTest struct {
	name      string
	offset    int
	expectErr bool
	expected  string
}

func TestSetGlobalTimezoneOffset(t *testing.T) {
	tests := []setTimezoneOffsetTest{
		{
			name:      "positive offset",
			offset:    7200,
			expectErr: false,
			expected:  "UTC+02:00",
		},
		{
			name:      "negative offset",
			offset:    -18000,
			expectErr: false,
			expected:  "UTC-05:00",
		},
		{
			name:      "zero offset",
			offset:    0,
			expectErr: false,
			expected:  "UTC+00:00",
		},
		{
			name:      "very large offset",
			offset:    math.MaxInt64,
			expectErr: true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := SetGlobalTimezoneOffset(test.offset)
			if test.expectErr {
				assert.Error(t, err, "SetGlobalTimezoneOffset should return an error for offset %d", test.offset)
			} else {
				assert.NoError(t, err, "SetGlobalTimezoneOffset should not return an error for offset %d", test.offset)
				assert.Equal(t, test.expected, time.Now().Format("UTC-07:00"), "Current time is not in expected timezone for offset %d", test.offset)
			}
		})
	}

	t.Run("no error with valid timezone name", func(t *testing.T) {
		err := SetGlobalTimezoneOffset(-14400)
		assert.NoError(t, err, "SetGlobalTimezoneOffset should not return an error for valid timezone name")
	})

	t.Run("error with invalid offset", func(t *testing.T) {
		err := SetGlobalTimezoneOffset(86400 + 1)
		assert.Error(t, err, "SetGlobalTimezoneOffset should return an error for an offset greater than or equal to 86400")
	})
}
