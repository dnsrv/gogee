package dcfg

import (
	"fmt"
	"math"
	"time"
)

// SetGlobalTimezoneOffset sets the global timezone offset for all time-related operations
func SetGlobalTimezoneOffset(offset int) error {
	// Check if the offset is valid
	if offset < -86400 || offset > 86400 {
		return fmt.Errorf("offset %d is invalid", offset)
	}

	// Calculate the absolute value of the offset
	absOffset := int(math.Abs(float64(offset)))

	// Determine the sign of the offset
	sign := "+"
	if offset < 0 {
		sign = "-"
	}

	// Construct the timezone name in the format "UTC+HH:MM" or "UTC-HH:MM"
	tzName := fmt.Sprintf("UTC%s%02d:%02d", sign, absOffset/3600, (absOffset%3600)/60)

	// Set the new timezone as the default timezone for all time-related operations
	time.Local = time.FixedZone(tzName, offset)

	// Verify that the new timezone is correct by checking the current time
	_, err := time.Parse("2006-01-02 15:04:05", time.Now().Format("2006-01-02 15:04:05"))
	if err != nil {
		return err
	}

	return nil
}
