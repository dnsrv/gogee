package utils

import (
	"crypto/md5"
	rand2 "crypto/rand"
	"fmt"
	"math/rand"
	"time"
)


// PseudoUuidGenerator
// returns UUID-like string
func PseudoUuidGenerator() (uuid string) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		fmt.Println("Error: ", err)
		return ""
	}

	uuid = fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])
	return
}

// RandToken
// returns string like 67c298ad00bff537fcc5ef6855bc8530
func RandToken() string {
	b := make([]byte, 8)
	_, _ = rand2.Read(b)
	t, _ := time.Now().MarshalBinary()
	return fmt.Sprintf("%x", md5.Sum(append(b, t...)))
}

func RandomString(n int) string {
	letterRunes := []rune("bdfghijmnqrstuvwz")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func HashSum(data string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(data)))
}