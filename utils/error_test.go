package utils

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestAppError(t *testing.T) {
	message := "item validation failed"
	details := "item name is too long"
	innerErr := sql.ErrConnDone
	err := &AppError{message, details, innerErr}

	assert.NotNil(t, err)

	assert.Equal(t, fmt.Sprintf("%s%s%s", message, HRDelimiter, details), err.Error())
	assert.Equal(t, fmt.Sprintf("%s%s%s%s%s", message, HRDelimiter, details, WEDelimiter, innerErr),
		errors.Unwrap(err).Error())

	assert.True(t, strings.HasPrefix(err.Error(), "item validation failed"))

	fmt.Printf("[test] public : %s\n", err)
	fmt.Printf("[test] private: %s\n", errors.Unwrap(err))
}