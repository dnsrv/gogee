package utils

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPseudoUuidGenerator(t *testing.T) {
	uuid := PseudoUuidGenerator()
	assert.Equal(t, 36, len(uuid))
	//assert.Equal(t, 4, strings.Count("-", uuid))
}

func TestRandomString(t *testing.T) {
	length := 64
	str := RandomString(length)
	assert.Equal(t, length, len(str))
}