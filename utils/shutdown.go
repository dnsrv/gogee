package utils

import (
	"io"
	"log"
	"os"
	"os/signal"
)

func CloseOnSignal(signals []os.Signal) chan io.Closer {
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, signals...)
	itemsChan := make(chan io.Closer, 20)

	go func() {
		sig := <-sigChan
		log.Printf("caught signal %s. shutting down...\r\n", sig)

		for item := range itemsChan {
			if err := item.Close(); err != nil {
				log.Printf("failed to close %v: %v\r\n", item, err)
			}
		}
	}()

	return itemsChan
}
