package utils

import "fmt"

var (
	HRDelimiter = ": "
	WEDelimiter = " >> "
)

type AppError struct {
	Message string
	Details string
	WrapErr error
}

func NewError(inner error, outer string) *AppError {
	return &AppError{
		WrapErr: inner,
		Message: outer,
	}
}

func (o *AppError) Unwrap() error {
	if o.WrapErr == nil {
		return fmt.Errorf("%s", o.getMessage())
	}
	return fmt.Errorf("%s%s%w", o.getMessage(), WEDelimiter, o.WrapErr)
}

func (o *AppError) Error() string {
	return o.getMessage()
}

func (o *AppError) getMessage() string {
	if o.Details != "" {
		return fmt.Sprintf("%s%s%s", o.Message, HRDelimiter, o.Details)
	}

	return o.Message
}

// ----------------------------------------------------------------------------

