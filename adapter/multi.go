package adapter

import (
	"database/sql"
	"time"
)

type MultiAdapter struct {
	connections map[string]*sql.DB
	Timeout     time.Duration
	Retry       int
	connChan    chan *sql.DB
	isRunning   bool
}

func NewMulti2(conns map[string]*sql.DB) *MultiAdapter {
	a := &MultiAdapter{
		Timeout:     3 * time.Second,
		Retry:       3,
		isRunning:   true,
		connections: conns,
		connChan:    make(chan *sql.DB),
	}

	go a.run()
	return a
}

func (o *MultiAdapter) run() {
	o.isRunning = true
	for o.isRunning {
		for _, con := range o.connections {
			o.connChan <- con
		}
	}
}

func (o *MultiAdapter) GetConnection() (*sql.DB, error) {
	conn, ok := <-o.connChan
	if !ok {
		return nil, nil
	}

	for i := 0; i < o.Retry; i++ {
		if err := conn.Ping(); err == nil {
			return conn, nil
		}
		time.Sleep(o.Timeout)
	}

	return nil, nil
}

func (o *MultiAdapter) Close() error {
	o.isRunning = false
	close(o.connChan)
	for _, conn := range o.connections {
		conn.Close()
	}
	return nil
}
