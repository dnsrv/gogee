package adapter

import (
	"context"
	"database/sql"
	"fmt"
	"sync"
	"time"
)

type Adapter struct {
	mu        sync.Mutex
	db        *sql.DB
	connector func() (*sql.DB, error)
	Timeout   time.Duration
	Retry     int
	OnError   func(code string, message string)
	OnDebug   func(code string, message string)
}

func New(connector func() (*sql.DB, error)) *Adapter {
	return &Adapter{
		mu:        sync.Mutex{},
		connector: connector,
		Timeout:   3 * time.Second,
		Retry:     3,
		OnError:   func(code string, message string) {},
		OnDebug:   func(code string, message string) {},
	}
}

func (o *Adapter) GetConnectionFor(usedBy string) (*sql.DB, error) {
	o.mu.Lock()
	defer o.mu.Unlock()

	if o.db != nil {
		ctx, cancel := context.WithTimeout(context.Background(), o.Timeout)
		defer cancel()
		err := o.db.PingContext(ctx)
		if err == nil && o.db != nil {
			o.OnDebug("GAC001", fmt.Sprintf("adapter: reusing connection. ping ok. [%s]", usedBy))
			return o.db, nil
		}
		o.OnError("GAC021", fmt.Sprintf("adapter: could not ping old connection [%s]: %s", usedBy, err.Error()))

		if o.db != nil {
			o.db.Close()
		}
		o.db = nil
	}

	o.OnDebug("GAC002", fmt.Sprintf("adapter: getting new connection [%s]", usedBy))

	var err error
	for i := 0; i < o.Retry; i++ {
		o.db, err = o.connector()
		if err == nil {
			o.OnDebug("GAC003", fmt.Sprintf("adapter: new connection established [%s]", usedBy))
			o.db.SetMaxOpenConns(10)
			o.db.SetMaxIdleConns(5)
			o.db.SetConnMaxLifetime(30 * time.Minute)
			ctx, cancel := context.WithTimeout(context.Background(), o.Timeout)
			defer cancel()
			inerr := o.db.PingContext(ctx)
			if inerr == nil && o.db != nil {
				o.OnDebug("GAC004", fmt.Sprintf("adapter: new connection pinged [%s]", usedBy))
				return o.db, nil
			}

			o.OnError("GAC024", fmt.Sprintf("adapter: could not ping new connection [%s]: %s", usedBy, inerr.Error()))
		}

		if o.db != nil {
			o.db.Close()
			o.db = nil
		}

		o.OnError("GAC022", fmt.Sprintf("adapter: could not get new connection [%s]: %s", usedBy, err.Error()))
		time.Sleep(200 * time.Millisecond)
	}
	if o.db != nil {
		o.db.Close()
	}
	o.db = nil

	return nil, err
}

func (o *Adapter) GetConnection() (*sql.DB, error) {
	o.mu.Lock()
	defer o.mu.Unlock()

	if o.db != nil {
		ctx, cancel := context.WithTimeout(context.Background(), o.Timeout)
		defer cancel()
		err := o.db.PingContext(ctx)
		if err == nil && o.db != nil {
			o.OnDebug("GAC001", "adapter: reusing connection. ping ok")
			return o.db, nil
		}
		o.OnError("GAC021", fmt.Sprintf("adapter: could not ping old connection: %s", err.Error()))

		if o.db != nil {
			o.db.Close()
		}
		o.db = nil
	}

	o.OnDebug("GAC002", "adapter: getting new connection")

	var err error
	for i := 0; i < o.Retry; i++ {
		o.db, err = o.connector()
		if err == nil {
			o.OnDebug("GAC003", "adapter: new connection established")
			o.db.SetMaxOpenConns(10)
			o.db.SetMaxIdleConns(5)
			o.db.SetConnMaxLifetime(30 * time.Minute)
			ctx, cancel := context.WithTimeout(context.Background(), o.Timeout)
			defer cancel()
			inerr := o.db.PingContext(ctx)
			if inerr == nil && o.db != nil {
				o.OnDebug("GAC004", "adapter: new connection pinged")
				return o.db, nil
			}

			o.OnError("GAC024", fmt.Sprintf("adapter: could not ping new connection: %s", inerr.Error()))
		}

		if o.db != nil {
			o.db.Close()
			o.db = nil
		}

		o.OnError("GAC022", fmt.Sprintf("adapter: could not get new connection: %s", err.Error()))
		time.Sleep(200 * time.Millisecond)
	}
	if o.db != nil {
		o.db.Close()
	}
	o.db = nil

	return nil, err
}

func (o *Adapter) Close() error {
	if o.db == nil {
		return nil
	}
	err := o.db.Close()
	o.db = nil
	return err
}
