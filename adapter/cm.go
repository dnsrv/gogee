package adapter

import (
	"database/sql"
	"fmt"
	"sync"
	"time"
)

type MultiAdapter3 struct {
	connections map[string]*sql.DB
	Timeout     time.Duration
	Retry       int
	connChan    chan *sql.DB
	isRunning   bool
	wg          sync.WaitGroup
}

func NewMulti(conns map[string]*sql.DB, timeout time.Duration, retry int) *MultiAdapter3 {
	a := &MultiAdapter3{
		Timeout:     timeout,
		Retry:       retry,
		isRunning:   true,
		connections: conns,
		connChan:    make(chan *sql.DB),
	}

	go a.run()
	return a
}

func (o *MultiAdapter3) run() {
	defer close(o.connChan)
	for _, con := range o.connections {
		o.wg.Add(1)
		o.connChan <- con
		o.wg.Done()
	}
}

func (o *MultiAdapter3) GetConnection() (*sql.DB, error) {
	conn, ok := <-o.connChan
	if !ok {
		return nil, fmt.Errorf("no available connections")
	}

	for i := 0; i < o.Retry; i++ {
		if err := conn.Ping(); err == nil {
			return conn, nil
		}
		time.Sleep(o.Timeout)
	}

	return o.GetConnection()
}

func (o *MultiAdapter3) Close() error {
	if !o.isRunning {
		return nil
	}

	o.isRunning = false
	close(o.connChan)
	o.wg.Wait()

	for _, conn := range o.connections {
		conn.Close()
	}

	return nil
}
