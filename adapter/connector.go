package adapter

import (
	"database/sql"
	"fmt"

	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/lib/pq"
	_ "modernc.org/sqlite"
)

// NewSqliteConnector returns a function that creates a new sqlite connection
// filepath or :memory:
func NewSqliteConnector(dbPath string) func() (*sql.DB, error) {
	return func() (*sql.DB, error) {
		db, err := sql.Open("sqlite", dbPath)
		if err != nil {
			return nil, fmt.Errorf("failed to open connection: %v", err)
		}

		_, err = db.Exec("SELECT 1")
		if err != nil {
			return nil, fmt.Errorf("failed to access database file: %v", err)
		}

		return db, nil
	}
}

// NewPostgresConnector returns a function that creates a new postgres connection
// user=%s password=%s dbname=%s sslmode=disable
func NewPostgresConnector(connStr string) func() (*sql.DB, error) {
	return func() (*sql.DB, error) {
		db, err := sql.Open("postgres", connStr)
		if err != nil {
			return nil, fmt.Errorf("failed to open connection: %v", err)
		}

		err = db.Ping()
		if err != nil {
			return nil, fmt.Errorf("failed to ping database: %v", err)
		}

		rows, err := db.Query("SELECT 1")
		if err != nil {
			return nil, fmt.Errorf("failed to execute query: %v", err)
		}
		defer rows.Close()

		return db, nil
	}
}

// NewMssqlConnector returns a function that creates a new mssql connection
// server=%s;user id=%s;password=%s;database=%s
func NewMssqlConnector(connStr string) func() (*sql.DB, error) {
	return func() (*sql.DB, error) {
		db, err := sql.Open("mssql", connStr)
		if err != nil {
			return nil, fmt.Errorf("failed to open connection: %v", err)
		}

		err = db.Ping()
		if err != nil {
			return nil, fmt.Errorf("failed to ping database: %v", err)
		}

		rows, err := db.Query("SELECT 1")
		if err != nil {
			return nil, fmt.Errorf("failed to execute query: %v", err)
		}
		defer rows.Close()

		return db, nil
	}
}
