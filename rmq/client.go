package rmq

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/google/uuid"
	// "github.com/streadway/amqp" // Deprecated. Use the one below
	amqp "github.com/rabbitmq/amqp091-go"
)

type Direction string

const (
	RECIEVING Direction = "recieving"
	SENDING   Direction = "sending"
)

type ILogHandler interface {
	Debug(code string, msg string)
	Info(code string, msg string)
	Warn(code string, msg string)
	Error(code string, msg string)
}

type RabbitConfig struct {
	User              string
	Pass              string
	Host              string
	Port              string
	Product           string
	Version           string
	ConnectionName    string
	ReconnectInterval time.Duration
}

type RabbitClient struct {
	connM               sync.Mutex
	sendingConnection   *amqp.Connection
	recievingConnection *amqp.Connection
	sendingChannel      *amqp.Channel
	recievingChannel    *amqp.Channel
	cfg                 RabbitConfig
	lh                  ILogHandler
	stop                bool
	stopRecChan         chan bool
}

func NewRabbitClient(cfg RabbitConfig, lh ILogHandler) *RabbitClient {
	if cfg.ReconnectInterval == 0 {
		cfg.ReconnectInterval = 5 * time.Second
	}

	return &RabbitClient{
		connM:       sync.Mutex{},
		cfg:         cfg,
		lh:          lh,
		stopRecChan: make(chan bool, 1),
	}
}

// Connect connects to rabbitmq
func (o *RabbitClient) connect(dir Direction, reconnect bool, reason string) (*amqp.Connection, error) {
	if reconnect {
		if dir == RECIEVING {
			if o.recievingChannel != nil {
				o.recievingChannel.Close()
			}
			o.recievingChannel = nil

			if o.recievingConnection != nil {
				_ = o.recievingConnection.Close()
			}
			o.recievingConnection = nil

		} else {
			if o.sendingChannel != nil {
				o.sendingChannel.Close()
			}
			o.sendingChannel = nil

			if o.sendingConnection != nil {
				_ = o.sendingConnection.Close()
			}
			o.sendingConnection = nil
		}
	}
	o.connM.Lock()
	defer o.connM.Unlock()

	if dir == RECIEVING && o.recievingConnection != nil {
		return o.recievingConnection, nil
	}

	if dir != RECIEVING && o.sendingConnection != nil {
		return o.sendingConnection, nil
	}

	url := fmt.Sprintf("amqp://%s:%s@%s:%s/", o.cfg.User, o.cfg.Pass, o.cfg.Host, o.cfg.Port)

	conn, err := amqp.DialConfig(url, amqp.Config{
		Properties: amqp.Table{
			"connection_name": fmt.Sprintf("%s::%s.%s", dir, o.cfg.ConnectionName, uuid.New().String()[:8]),
			"product":         o.cfg.Product,
			"version":         o.cfg.Version,
		},
	})
	if err != nil {
		o.lh.Error("RMQ101", fmt.Sprintf("could not connect to rabbitmq: %s", err.Error()))
		time.Sleep(1 * time.Second)
		return nil, err
	}

	o.lh.Debug("RMQ001", fmt.Sprintf("new %s conn to %s", dir, reason))

	switch dir {
	case RECIEVING:
		o.recievingConnection = conn
		return o.recievingConnection, nil
	default:
		o.sendingConnection = conn
		return o.sendingConnection, nil
	}
}

// openChannel opens channel.
// Instead of creating new connection for each channel, we create one connection for each direction
func (o *RabbitClient) openChannel(dir Direction, recreate bool, reason string) (*amqp.Channel, error) {

	if recreate {
		if dir == RECIEVING {
			if o.recievingChannel != nil {
				o.recievingChannel.Close()
			}

			o.recievingChannel = nil
		} else {
			if o.sendingChannel != nil {
				o.sendingChannel.Close()
			}
			o.sendingChannel = nil
		}
	}
	if dir == RECIEVING && o.recievingConnection == nil {
		o.recievingChannel = nil
	}
	if dir != RECIEVING && o.sendingConnection == nil {
		o.recievingChannel = nil
	}
	if dir == RECIEVING && o.recievingChannel != nil {
		return o.recievingChannel, nil
	} else if dir != RECIEVING && o.sendingChannel != nil {
		return o.sendingChannel, nil
	}

	for {
		_, err := o.connect(dir, recreate, reason)
		if err == nil {
			break
		}
	}

	var err error
	if dir == RECIEVING {
		o.recievingChannel, err = o.recievingConnection.Channel()
	} else {
		o.sendingChannel, err = o.sendingConnection.Channel()
	}
	if err != nil {
		o.lh.Warn("RMQ102", fmt.Sprintf("could not create channel: %s", err.Error()))
		time.Sleep(o.cfg.ReconnectInterval)
		return nil, err
	}

	o.lh.Debug("RMQ002", fmt.Sprintf("new %s chann to %s", dir, reason))

	if dir == RECIEVING {
		return o.recievingChannel, err
	} else {
		return o.sendingChannel, err
	}
}

// Close closes rabbitmq client
func (o *RabbitClient) Close() error {
	if o.stop {
		return nil
	}

	o.lh.Debug("RMQ003", "closing rabbitmq client")

	o.stop = true
	o.stopRecChan <- true

	if o.recievingConnection != nil {
		_ = o.recievingConnection.Close()
		o.recievingConnection = nil
	}

	if o.sendingConnection != nil {
		_ = o.sendingConnection.Close()
		o.sendingConnection = nil
	}

	o.lh.Info("RMQ004", "rabbitmq client closed")
	return nil
}

// Consume consumes messages from queue
func (o *RabbitClient) Consume(ctx context.Context, qName string, prefetch int, dispatcher func([]byte) error) {
	for !o.stop {

		for {
			_, err := o.openChannel(RECIEVING, true, fmt.Sprintf("consume from '%s'", qName))
			if err == nil {
				break
			}
		}

		err := o.recievingChannel.Qos(prefetch, 0, false)
		if err != nil {
			o.lh.Warn("RMQ009", fmt.Sprintf("can't set qos: %s", err.Error()))
			continue
		}

		m, err := o.recievingChannel.Consume(
			qName,
			uuid.New().String(),
			false,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			o.lh.Warn("RMQ005", fmt.Sprintf("can't consume from '%s': %s", qName, err.Error()))
			continue
		}

		o.lh.Debug("RMQ003", fmt.Sprintf("ready to consume from '%s'", qName))

		connClose := o.recievingConnection.NotifyClose(make(chan *amqp.Error))
		connBlocked := o.recievingConnection.NotifyBlocked(make(chan amqp.Blocking))
		chClose := o.recievingChannel.NotifyClose(make(chan *amqp.Error))

	Process:
		for !o.stop {
			select {
			case <-o.stopRecChan:
				o.lh.Debug("RMQ025", "stop recieving")
				break Process
			case res := <-connBlocked:
				o.lh.Warn("RMQ006", fmt.Sprintf("connection blocked: %s", res.Reason))
				break Process
			case err = <-connClose:
				o.lh.Warn("RMQ007", fmt.Sprintf("connection closed: %s", err))
				break Process
			case err = <-chClose:
				o.lh.Warn("RMQ008", fmt.Sprintf("channel closed: %s", err))
				break Process
			case d := <-m:
				if d.Body == nil || len(d.Body) == 0 {
					_ = d.Reject(false)
					continue
				}

				err := dispatcher(d.Body)
				if err != nil {
					_ = d.Reject(false)
					continue
				}

				_ = d.Ack(true)
			}
		}
	}
}

// Publish publishes message to exchange
func (o *RabbitClient) Publish(ctx context.Context, qName string, data []byte) error {
	reconnect := false
	retry := 3
	for i := 0; i < retry; i++ {
		for {
			_, err := o.openChannel(SENDING, reconnect, fmt.Sprintf("publish to '%s'", qName))
			if err == nil {
				break
			}
		}

		err := o.sendingChannel.PublishWithContext(
			ctx,
			"",
			qName,
			false,
			false,
			amqp.Publishing{
				MessageId:    uuid.New().String(),
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         data,
			})
		if err != nil {
			o.lh.Warn("RMQ010", fmt.Sprintf("can't publish to '%s' - %s", qName, err.Error()))
			reconnect = true
			time.Sleep(o.cfg.ReconnectInterval)
			continue
		}
		return nil
	}

	return fmt.Errorf("failed to publish to queue after %d retries", retry)
}
