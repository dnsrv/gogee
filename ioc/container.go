package ioc

import (
	"context"
	"fmt"
	"sync"
)

// Invoker is a function that returns interface instance
type Invoker func(c Container) (interface{}, error)

// Closer will be called on container close
type Closer func(ctx context.Context, c Container) error

// Container is a DI container interface
type Container interface {
	Register(dep string, depFunc Invoker)
	Resolve(dep string) (interface{}, error)
	OnClose(tdf Closer)
	Dump() []string
	Close(context.Context) error
}

// diContainer is a DI container implementation that uses maps
type diContainer struct {
	mapM       sync.RWMutex
	invokers   map[string]Invoker
	instances  map[string]interface{}
	closeFuncs []Closer
}

// NewContainer returns new diContainer
func NewContainer() Container {
	return &diContainer{
		mapM:       sync.RWMutex{},
		invokers:   make(map[string]Invoker),
		instances:  make(map[string]interface{}),
		closeFuncs: make([]Closer, 0),
	}
}

// Register adds new dep to container: implemented interface name and Invoker that returns interface instance
func (o *diContainer) Register(dep string, i Invoker) {
	o.invokers[dep] = i
}

// Resolve returns depedency by interface name or error
func (o *diContainer) Resolve(dep string) (interface{}, error) {
	// check if already resolved
	if obj, ok := o.instances[dep]; ok {
		return obj, nil
	}

	// check invoker exists
	i, ok := o.invokers[dep]
	if !ok {
		return nil, fmt.Errorf("invoker not found: [%s]", dep)
	}

	// invoke new instance
	obj, err := i(o)
	if err != nil {
		return nil, err
	}

	// check if returned object is nil
	if obj == nil {
		return nil, fmt.Errorf("invoker returned nil: [%s]", dep)
	}

	// save instance
	o.instances[dep] = obj
	return obj, nil
}

// OnClose registeres teardown function that will be called on Close()
func (o *diContainer) OnClose(tdf Closer) {
	o.closeFuncs = append(o.closeFuncs, tdf)
}

// Dump returns all registered dependencies
func (o *diContainer) Dump() []string {
	var deps []string
	for key := range o.invokers {
		deps = append(deps, key)
	}
	return deps
}

// Close calls Close() method of all dependencies that implements io.Closer
func (o *diContainer) Close(ctx context.Context) error {
	var lastErr error

	// reversal loop to call Close() in reverse order of registration
	for i := len(o.closeFuncs) - 1; i >= 0; i-- {
		err := o.closeFuncs[i](ctx, o)
		if err != nil {
			lastErr = err
		}
	}

	return lastErr
}
