package ioc

import (
	"fmt"
	"strings"
)

// Register saves depedency by interface name and Invoker that returns interface instance
func Register[T any](c Container, i Invoker) {
	c.Register(toString[T](), i)
}

// TryResolve returns depedency by interface name or error
func TryResolve[T any](c Container) (T, error) {
	obj, err := c.Resolve(toString[T]())
	if err != nil {
		return empty[T](), err
	}
	return obj.(T), nil
}

// MustResolve returns depedency by interface name or panic
func MustResolve[T any](c Container) T {
	obj, err := TryResolve[T](c)
	if err != nil {
		err = fmt.Errorf("%s. \r\n\tAvailable deps: \r\n%s", err.Error(), strings.Join(c.Dump(), "\r\n"))
		panic(err)
	}
	return obj
}

// empty returns empty instance of T
func empty[T any]() (t T) {
	return
}

// generateServiceName returns interface name as string
func toString[T any]() string {
	var t T
	// struct
	name := fmt.Sprintf("%T", t)
	if name != "<nil>" {
		return name
	}
	// interface
	return fmt.Sprintf("%T", new(T))
}
