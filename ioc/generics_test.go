package ioc

import (
	"context"
	"fmt"
	"io"
	"testing"
)

func TestGenerics(t *testing.T) {
	tests := genericTestSuite{}
	t.Run("register", tests.register)
	t.Run("tryResolve", tests.tryResolve)
	t.Run("mustResolve", tests.mustResolve)
	t.Run("empty", tests.empty)
	t.Run("toString", tests.toString)
}

type genericTestSuite struct{}

func (o *genericTestSuite) register(t *testing.T) {
	mockContainer := &MockContainer{}

	// Register a mock invoker
	Register[string](mockContainer, func(c Container) (interface{}, error) {
		return "Hello, world!", nil
	})

	// Check if the invoker was registered with the correct interface name
	if _, ok := mockContainer.registry["string"]; !ok {
		t.Errorf("Expected dependency with interface name 'string' to be registered")
	}
}

func (o *genericTestSuite) tryResolve(t *testing.T) {
	// Create a mock container
	mockContainer := &MockContainer{}

	// Register a mock invoker
	Register[string](mockContainer, func(c Container) (interface{}, error) {
		return "Hello, world!", nil
	})

	// Try resolving the dependency with the correct interface name
	result, err := TryResolve[string](mockContainer)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	// Check if the resolved result matches the expected value
	expected := "Hello, world!"
	if result != expected {
		t.Errorf("Expected resolved result to be '%s', but got '%s'", expected, result)
	}

	// Try resolving a non-existent dependency
	_, err = TryResolve[int](mockContainer)
	if err == nil {
		t.Error("Expected error, but got nil")
	}
}

func (o *genericTestSuite) mustResolve(t *testing.T) {
	// Create a mock container
	mockContainer := &MockContainer{}

	// Register a mock invoker
	Register[string](mockContainer, func(c Container) (interface{}, error) {
		return "Hello, world!", nil
	})

	// Call MustResolve with the correct interface name
	result := MustResolve[string](mockContainer)

	// Check if the resolved result matches the expected value
	expected := "Hello, world!"
	if result != expected {
		t.Errorf("Expected resolved result to be '%s', but got '%s'", expected, result)
	}

	// Call MustResolve with a non-existent dependency
	defer func() {
		if r := recover(); r == nil {
			t.Error("Expected panic, but got nil")
		}
	}()
	MustResolve[int](mockContainer)
}

func (o *genericTestSuite) empty(t *testing.T) {
	// Call empty with various types and check if it returns the zero value
	var result1 int
	var result2 string
	var result3 bool

	if empty[int]() != 0 {
		t.Errorf("Expected empty to return zero value for int, but got %v", result1)
	}
	if empty[string]() != "" {
		t.Errorf("Expected empty to return zero value for string, but got %v", result2)
	}
	if empty[bool]() != false {
		t.Errorf("Expected empty to return zero value for bool, but got %v", result3)
	}
}

func (o *genericTestSuite) toString(t *testing.T) {
	// Call toString with various types and check if it returns the correct string representation

	if res := toString[MockContainer](); res != "ioc.MockContainer" {
		t.Errorf("Expected toString to return string representation for MockContainer, but got %v", res)
	}
	if res := toString[*MockContainer](); res != "*ioc.MockContainer" {
		t.Errorf("Expected toString to return string representation for *MockContainer, but got %v", res)
	}

	if res := toString[io.Closer](); res != "*io.Closer" {
		t.Errorf("Expected toString to return string representation for *io.Closer, but got %v", res)
	}

	if res := toString[string](); res != "string" {
		t.Errorf("Expected toString to return string representation for string, but got %v", res)
	}

	if res := toString[bool](); res != "bool" {
		t.Errorf("Expected toString to return string representation for bool, but got %v", res)
	}
}

// ----------------------------------------------------------------------------

type MockContainer struct {
	registry map[string]Invoker
}

func (c *MockContainer) Register(name string, i Invoker) {
	if c.registry == nil {
		c.registry = make(map[string]Invoker)
	}
	c.registry[name] = i
}

func (c *MockContainer) Resolve(name string) (interface{}, error) {
	i, ok := c.registry[name]
	if !ok {
		return nil, fmt.Errorf("dependency not found: %s", name)
	}
	return i(c)
}

func (c *MockContainer) Dump() []string {
	var keys []string
	for k := range c.registry {
		keys = append(keys, k)
	}
	return keys
}

func (c *MockContainer) OnClose(tdf Closer) {}

func (c *MockContainer) Close(_ context.Context) error {
	return nil
}
