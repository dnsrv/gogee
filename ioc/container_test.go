package ioc

import (
	"context"
	"fmt"
	"testing"
)

func TestContainer(t *testing.T) {
	tests := containerTestSuite{}
	t.Run("register", tests.register)
	t.Run("resolve", tests.resolve)
	t.Run("onClose", tests.onClose)
	t.Run("close", tests.close)
	t.Run("dump", tests.dump)
}

type containerTestSuite struct{}

func (o *containerTestSuite) register(t *testing.T) {
	container := NewContainer()

	// Register a mock invoker
	container.Register("string", func(c Container) (interface{}, error) {
		return "Hello, world!", nil
	})

	// Check if the invoker was registered with the correct interface name
	if _, ok := container.(*diContainer).invokers["string"]; !ok {
		t.Errorf("Expected dependency with interface name 'string' to be registered")
	}
}

func (o *containerTestSuite) resolve(t *testing.T) {
	t.Run("resolve", func(t *testing.T) {
		// Create a mock container
		container := NewContainer()

		// Register a mock invoker
		container.Register("string", func(c Container) (interface{}, error) {
			return "Hello, world!", nil
		})

		// Resolve the dependency with the correct interface name
		result, err := container.Resolve("string")
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}

		// Check if the resolved result matches the expected value
		expected := "Hello, world!"
		if result != expected {
			t.Errorf("Expected resolved result to be '%s', but got '%s'", expected, result)
		}

		// Resolve a non-existent dependency
		_, err = container.Resolve("int")
		if err == nil {
			t.Error("Expected error, but got nil")
		}

		// check o.instances[dep] = obj
		length := len(container.(*diContainer).instances)
		if length != 1 {
			t.Errorf("Expected instances to have length 1, but got %d", length)
		}
	})
	t.Run("resolveAgain", func(t *testing.T) {
		// Create a mock container
		container := NewContainer()
		dep := &mockDep{ID: 1}
		counter := 99
		// Register a mock invoker
		container.Register("dep", func(c Container) (interface{}, error) {
			counter += 1
			dep.ID = counter
			return dep, nil
		})

		// Resolve the dependency with the correct interface name
		result, err := container.Resolve("dep")
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}

		// Check if the resolved result matches the expected value
		if result != dep {
			t.Errorf("Expected resolved result to be '%T', but got '%T'", dep, result)
		}

		// Resolve the dependency again
		result, err = container.Resolve("dep")
		if err != nil {
			t.Errorf("Unexpected error: %v", err)
		}

		// cast the result to the expected type
		dep, ok := result.(*mockDep)
		if !ok {
			t.Errorf("Expected result to be of type '%T', but got '%T'", dep, result)
		}

		// Check dep.ID to see if it was resolved again
		if dep.ID != counter {
			t.Errorf("Expected dep.ID to be 100, but got %d", dep.ID)
		}
	})
	t.Run("invoker error", func(t *testing.T) {
		errorExpected := fmt.Errorf("error expected")
		// Create a mock container
		container := NewContainer()

		// Register a mock invoker
		container.Register("string", func(c Container) (interface{}, error) {
			return nil, errorExpected
		})

		// Resolve the dependency with the correct interface name
		_, err := container.Resolve("string")
		if err == nil {
			t.Error("Expected error, but got nil")
		}
	})
	t.Run("invoker nil", func(t *testing.T) {
		// Create a mock container
		container := NewContainer()

		// Register a mock invoker
		container.Register("string", func(c Container) (interface{}, error) {
			return nil, nil
		})

		// Resolve the dependency with the correct interface name
		_, err := container.Resolve("string")
		if err == nil {
			t.Error("Expected error, but got nil")
		}
	})
}

func (o *containerTestSuite) onClose(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		// Create a mock container
		container := NewContainer()

		// Register a mock closer
		container.OnClose(func(ctx context.Context, c Container) error {
			return nil
		})

		// Check if the closer was registered
		if len(container.(*diContainer).closeFuncs) != 1 {
			t.Error("Expected closer to be registered")
		}
	})
	t.Run("error", func(t *testing.T) {
		errorExpected := fmt.Errorf("error expected")
		// Create a mock container
		container := NewContainer()

		// Register a mock closer
		container.OnClose(func(ctx context.Context, c Container) error {
			return errorExpected
		})

		err := container.Close(context.Background())
		if err != errorExpected {
			t.Errorf("Expected error %s, but got %s", errorExpected, err)
		}
	})
}

func (o *containerTestSuite) close(t *testing.T) {
	// Create a mock container
	container := NewContainer()

	// Register new dep
	container.Register("string", func(c Container) (interface{}, error) {
		// Register a mock closer
		container.OnClose(func(ctx context.Context, c Container) error {
			return nil
		})

		return "Hello, world!", nil
	})

	// Close the container
	container.Close(context.Background())

	// Check if the closer was called
	if len(container.(*diContainer).closeFuncs) != 0 {
		t.Error("Expected closer to be called")
	}
}

func (o *containerTestSuite) dump(t *testing.T) {
	// Create a mock container
	container := NewContainer()

	// Register a mock invoker
	container.Register("string", func(c Container) (interface{}, error) {
		return "Hello, world!", nil
	})

	// Dump the container
	dump := container.Dump()

	// Check if the dump contains the correct interface name
	found := false
	for _, v := range dump {
		if v == "string" {
			found = true
			break
		}
	}

	if !found {
		t.Error("Expected dump to contain interface name 'string'")
	}
}

type mockDep struct {
	ID int
}
