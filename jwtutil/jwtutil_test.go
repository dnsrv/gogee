package jwtutil

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestJwtUtil(t *testing.T) {
	k2100, err := ReadPrivateKeyFromFile("testdata/jwt.key")
	require.NoError(t, err)

	k2100public, err := ReadPublicKeyFromFile("testdata/jwt.key.pub")
	require.NoError(t, err)

	tests := jwtUtilTests{
		kp: &KeyPair{
			privateKey: k2100,
			publicKey:  k2100public,
		},
	}

	t.Run("TestAll", tests.TestAll)
}

type jwtUtilTests struct {
	kp *KeyPair
}

func (o *jwtUtilTests) TestAll(t *testing.T) {
	expireAt := time.Until(time.Date(2100, 1, 1, 0, 0, 0, 0, time.UTC))

	encoder := NewTokenEncoder(o.kp.privateKey)
	claims := DefaultClaims().
		WithPayload("foo", "bar").
		WithPayload("bar", "baz")

	claims.WithExpiration(expireAt)

	tokenStr, err := encoder.Encode(claims)
	require.NoError(t, err)
	require.NotEmpty(t, tokenStr)

	// decode the token
	decoder := NewTokenDecoder(o.kp.publicKey)
	token, err := decoder.ParseToken(tokenStr)
	require.NoError(t, err)

	// assert token is valid
	require.True(t, token.Valid)

	tokenPayload, err := token.ToPayload()
	require.NoError(t, err)
	require.True(t, len(tokenPayload) == 2)

	require.Equal(t, "bar", tokenPayload["foo"])
	require.Equal(t, "baz", tokenPayload["bar"])
}
