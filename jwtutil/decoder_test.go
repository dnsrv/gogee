package jwtutil

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestDecoder(t *testing.T) {
	kp, err := NewKeyPair()
	require.NoError(t, err)

	tests := decoderTests{
		kp: kp,
	}

	t.Run("TestParseToken", tests.TestParseToken)
}

type decoderTests struct {
	kp *KeyPair
}

func (o *decoderTests) TestParseToken(t *testing.T) {
	encoder := NewTokenEncoder(o.kp.privateKey)
	payload := map[string]interface{}{
		"foo": "bar",
	}

	t.Run("success", func(t *testing.T) {
		claims := DefaultClaims().
			WithPayload("foo", "bar").
			WithExpiration(time.Minute * 10)

		tokenStr, err := encoder.Encode(claims)
		require.NoError(t, err)
		require.NotEmpty(t, tokenStr)

		// t.Log(tokenStr)

		decoder := NewTokenDecoder(o.kp.publicKey)
		token, err := decoder.ParseToken(tokenStr)
		require.NoError(t, err)
		require.NotEmpty(t, token)

		// assert token is valid
		require.True(t, token.Valid)

		claimsPayload, err := token.ToPayload()
		require.NoError(t, err)
		require.True(t, len(claimsPayload) > 0)

		require.Equal(t, payload["foo"], claimsPayload["foo"])
	})
	// token is expired
	t.Run("expired", func(t *testing.T) {
		claims := DefaultClaims().
			WithPayload("foo", "bar").
			WithExpiration(time.Minute * -10)

		tokenStr, err := encoder.Encode(claims)
		require.NoError(t, err)
		require.NotEmpty(t, tokenStr)

		// t.Log(tokenStr)

		decoder := NewTokenDecoder(o.kp.publicKey)
		token, err := decoder.ParseToken(tokenStr)
		require.Nil(t, token)
		require.Error(t, err)
	})
}
