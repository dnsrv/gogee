package jwtutil

import (
	"testing"
	"time"

	jwt4 "github.com/dgrijalva/jwt-go/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestEncoder(t *testing.T) {
	kp, err := NewKeyPair()
	require.NoError(t, err)

	tests := EncoderTests{
		kp: kp,
	}

	t.Run("TestCreateToken", tests.TestCreateToken)
}

type EncoderTests struct {
	kp *KeyPair
}

func (o *EncoderTests) TestCreateToken(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		encoder := NewTokenEncoder(o.kp.privateKey)
		payload := map[string]interface{}{
			"foo": "bar",
		}

		claims := DefaultClaims().
			WithPayload("foo", "bar").
			WithExpiration(time.Minute * 10)

		tokenStr, err := encoder.Encode(claims)
		assert.NoError(t, err)
		assert.NotEmpty(t, tokenStr)

		// t.Log(tokenStr)

		// try to decode the token
		tokenParsed, err := jwt4.Parse(tokenStr, func(token *jwt4.Token) (interface{}, error) {
			return o.kp.publicKey, nil
		})

		assert.NoError(t, err)
		assert.True(t, tokenParsed.Valid)

		claimsPayload := ExtractPayload(tokenParsed.Claims)
		assert.True(t, len(claimsPayload) > 0)

		assert.Equal(t, payload["foo"], claimsPayload["foo"])
	})
}
