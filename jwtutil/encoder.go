package jwtutil

import (
	"crypto/rsa"

	jwt4 "github.com/dgrijalva/jwt-go/v4"
)

type TokenEncoder struct {
	privateKey *rsa.PrivateKey
}

func NewTokenEncoder(privateKey *rsa.PrivateKey) *TokenEncoder {
	return &TokenEncoder{
		privateKey: privateKey,
	}
}

func (o *TokenEncoder) Encode(claims *WriteClaims) (string, error) {
	return jwt4.NewWithClaims(jwt4.SigningMethodRS256, claims.MapClaims).
		SignedString(o.privateKey)
}
