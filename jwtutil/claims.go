package jwtutil

import (
	"time"

	jwt4 "github.com/dgrijalva/jwt-go/v4"
)

type WriteClaims struct {
	jwt4.MapClaims
}

// NewClaims creates a new claims
func DefaultClaims() *WriteClaims {
	return &WriteClaims{
		MapClaims: jwt4.MapClaims{
			"iat":     jwt4.At(time.Now()),
			"exp":     jwt4.At(time.Now().Add(time.Minute * 10)),
			"payload": make(map[string]interface{}),
		},
	}
}

// WithExpiration sets the expiration time
func (o *WriteClaims) WithExpiration(exp time.Duration) *WriteClaims {
	o.MapClaims["exp"] = jwt4.At(time.Now().Add(exp))
	return o
}

// WithIssuedAt sets the issued at time
func (o *WriteClaims) WithIssuedAt(issuedAt time.Time) *WriteClaims {
	o.MapClaims["iat"] = jwt4.At(issuedAt)
	return o
}

// WithIssuer sets the issuer
func (o *WriteClaims) WithIssuer(issuer string) *WriteClaims {
	o.MapClaims["iss"] = issuer
	return o
}

// WithSubject sets the subject
func (o *WriteClaims) WithSubject(subject string) *WriteClaims {
	o.MapClaims["sub"] = subject
	return o
}

// WithID sets the id
func (o *WriteClaims) WithID(id string) *WriteClaims {
	o.MapClaims["jti"] = id
	return o
}

// WithNotBefore sets the not before time
func (o *WriteClaims) WithNotBefore(notBefore time.Time) *WriteClaims {
	o.MapClaims["nbf"] = jwt4.At(notBefore)
	return o
}

// WithPayload adds a new key-value pair to the claims
func (o *WriteClaims) WithPayload(key string, value interface{}) *WriteClaims {
	// add the key-value pair to the payload
	o.MapClaims["payload"].(map[string]interface{})[key] = value
	return o
}

// ToMap converts the claims to a map
func (o *WriteClaims) ToMap() map[string]interface{} {
	return o.MapClaims
}

// ExtractPayload extracts the payload from the jwt4.WriteClaims
func ExtractPayload(claims jwt4.Claims) map[string]interface{} {
	if claims == nil {
		return make(map[string]interface{})
	}

	claimsMap, ok := claims.(jwt4.MapClaims)
	if !ok {
		return make(map[string]interface{})
	}

	payload, ok := claimsMap["payload"].(map[string]interface{})
	if !ok {
		return make(map[string]interface{})
	}

	return payload
}
