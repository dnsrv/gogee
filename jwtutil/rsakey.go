package jwtutil

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"io"
	"os"
)

// KeyPair represents a key pair (private and public)
type KeyPair struct {
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
}

// GetPrivateKey returns the private key
func (o *KeyPair) GetPrivateKey() *rsa.PrivateKey {
	return o.privateKey
}

// GetPublicKey returns the public key
func (o *KeyPair) GetPublicKey() *rsa.PublicKey {
	return o.publicKey
}

// ----------------------------------------------------------------------------

// NewKeyPair creates a new key pair
func NewKeyPair() (*KeyPair, error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}

	return &KeyPair{
		privateKey: privateKey,
		publicKey:  &privateKey.PublicKey,
	}, nil
}

// SaveKeyPair saves the key pair to the given file paths
func SaveKeyPair(k *KeyPair, privateKeyFilePath, publicKeyFilePath string) error {
	// save private key
	privateKeyFile, err := os.Create(privateKeyFilePath)
	if err != nil {
		return err
	}

	privateKeyPEM := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(k.privateKey),
	}

	err = pem.Encode(privateKeyFile, privateKeyPEM)
	if err != nil {
		return err
	}

	// save public key
	publicKeyFile, err := os.Create(publicKeyFilePath)
	if err != nil {
		return err
	}

	publicKeyPEM := &pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: x509.MarshalPKCS1PublicKey(k.publicKey),
	}

	err = pem.Encode(publicKeyFile, publicKeyPEM)
	if err != nil {
		return err
	}

	return nil
}

// ReadKeyPairFromFile reads the key pair from the given file paths
func ReadPrivateKeyFromFile(privateKeyFilePath string) (*rsa.PrivateKey, error) {
	privateKeyFile, err := os.Open(privateKeyFilePath)
	if err != nil {
		return nil, err
	}
	defer privateKeyFile.Close()

	pemData, err := io.ReadAll(privateKeyFile)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(pemData)
	privateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	return privateKey, nil
}

// ReadPublicKeyFromFile reads the key pair from the given file paths
func ReadPublicKeyFromFile(publicKeyFilePath string) (*rsa.PublicKey, error) {
	publicKeyFile, err := os.Open(publicKeyFilePath)
	if err != nil {
		return nil, err
	}
	defer publicKeyFile.Close()

	pemData, err := io.ReadAll(publicKeyFile)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(pemData)
	if block == nil {
		return nil, errors.New("failed to decode pem block")
	}

	publicKey, err := x509.ParsePKCS1PublicKey(block.Bytes)
	if err != nil {
		return nil, err
	}

	if publicKey == nil {
		return nil, errors.New("failed to parse public key")
	}

	return publicKey, nil
}
