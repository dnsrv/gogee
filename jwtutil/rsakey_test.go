package jwtutil

import (
	"testing"
)

func TestRsaLoad(t *testing.T) {

	kp, err := NewKeyPair()
	if err != nil {
		t.Errorf("NewKeyPair() error = %v", err)
		return
	}

	err = SaveKeyPair(kp, "testdata/jwt.key", "testdata/jwt.key.pub")
	if err != nil {
		t.Errorf("SaveKeyPair() error = %v", err)
		return
	}

	tests := RsaLoadTests{
		kp: kp,
	}

	t.Run("TestReadPrivateKeyFromFile", tests.TestReadPrivateKeyFromFile)
}

type RsaLoadTests struct {
	kp *KeyPair
}

func (o *RsaLoadTests) TestReadPrivateKeyFromFile(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		privateKey, err := ReadPrivateKeyFromFile("testdata/jwt.key")
		if err != nil {
			t.Error(err)
		}

		if privateKey == nil {
			t.Error("privateKey is nil")
		}

		if privateKey.PublicKey.E != o.kp.privateKey.PublicKey.E {
			t.Error("public key is not equal")
		}

		if privateKey.PublicKey.N.Cmp(o.kp.privateKey.PublicKey.N) != 0 {
			t.Error("public key is not equal")
		}

		if privateKey.D.Cmp(o.kp.privateKey.D) != 0 {
			t.Error("private key is not equal")
		}

		if privateKey.Primes[0].Cmp(o.kp.privateKey.Primes[0]) != 0 {
			t.Error("private key is not equal")
		}

		if privateKey.Primes[1].Cmp(o.kp.privateKey.Primes[1]) != 0 {
			t.Error("private key is not equal")
		}
	})
}
