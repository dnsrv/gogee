package jwtutil

import (
	"crypto/rsa"
	"errors"

	jwt4 "github.com/dgrijalva/jwt-go/v4"
)

type Token struct {
	jwt4.Token
}

type TokenDecoder struct {
	publicKey *rsa.PublicKey
}

func NewTokenDecoder(publicKey *rsa.PublicKey) *TokenDecoder {
	return &TokenDecoder{
		publicKey: publicKey,
	}
}

// ParseToken parses the token and returns the token
func (o *TokenDecoder) ParseToken(tokenString string) (*Token, error) {
	token, err := jwt4.Parse(tokenString, func(token *jwt4.Token) (interface{}, error) {
		return o.publicKey, nil
	})
	if err != nil {
		return nil, err
	}

	return &Token{
		Token: *token,
	}, nil
}

// ParseClaims parses the token and returns the claims
func (o *Token) ToPayload() (map[string]interface{}, error) {
	claims, ok := o.Claims.(jwt4.MapClaims)
	if !ok {
		return nil, errors.New("invalid claims")
	}

	payload, ok := claims["payload"].(map[string]interface{})
	if !ok {
		return nil, errors.New("invalid payload")
	}

	return payload, nil
}
