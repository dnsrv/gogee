package migmig

import (
	"database/sql"
	"fmt"
)

type SqliteMigrator struct {
	loader IMigLoader
}

func NewSqliteMigrator(ldr IMigLoader) *SqliteMigrator {
	return &SqliteMigrator{
		loader: ldr,
	}
}

func (o *SqliteMigrator) Apply(db *sql.DB) error {

	_, err := db.Exec(`
	CREATE TABLE IF NOT EXISTS migration_history (
		id SERIAL PRIMARY KEY,
		version INTEGER NOT NULL
	);`)
	if err != nil {
		return err
	}

	migrations, err := o.loader.Load()
	if err != nil {
		return err
	}

	for _, migration := range migrations {
		err := o.applyMigration(db, migration)
		if err != nil {
			return err
		}
	}

	return nil
}

func (o *SqliteMigrator) applyMigration(db *sql.DB, migration *Migration) error {
	alreadyApplied, err := o.isMigrationApplied(db, migration)
	if err != nil {
		return err
	}

	if alreadyApplied {
		fmt.Printf("Migration %d already applied.\n", migration.Version)
		return nil
	}

	fmt.Printf("Processing migration %d...\n", migration.Version)
	_, err = db.Exec(migration.Script)
	if err != nil {
		return err
	}

	_, err = db.Exec("INSERT INTO migration_history (version) VALUES (?)", migration.Version)
	if err != nil {
		return err
	}

	fmt.Printf("Migration %d successfully applied.\n", migration.Version)
	return nil
}

func (o *SqliteMigrator) isMigrationApplied(db *sql.DB, m *Migration) (bool, error) {
	var count int
	err := db.QueryRow("SELECT COUNT(*) FROM migration_history WHERE version = ?", m.Version).Scan(&count)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}
