package migmig

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"time"
)

type Migration struct {
	Name    string
	Version int
	Script  string
}

type IMigLoader interface {
	Load() ([]*Migration, error)
}

type Loader struct {
	FilenameRegex *regexp.Regexp
	Dir           string
}

func NewLoader(dir string) *Loader {
	return &Loader{
		Dir:           dir,
		FilenameRegex: regexp.MustCompile(`^(\d{4}-\d{2}-\d{2}_\d{2}-\d{2})_(.+)\.sql$`),
	}
}

// load migration files from directory and return []fs.FileInfo and error
func (o *Loader) GetFiles() ([]os.FileInfo, error) {
	// Открываем директорию
	dir, err := os.Open(o.Dir)
	if err != nil {
		fmt.Println("Ошибка при открытии директории:", err)
		return nil, err
	}
	defer dir.Close()

	// Читаем содержимое директории
	fileInfos, err := dir.Readdir(-1)
	if err != nil {
		fmt.Println("Ошибка при чтении директории:", err)
		return nil, err
	}

	return fileInfos, nil
}

func (o *Loader) Load() ([]*Migration, error) {
	var migrations []*Migration

	files, err := o.GetFiles()
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		// Пропускаем директории
		if file.IsDir() {
			continue
		}

		// Получаем имя файла
		fileName := file.Name()
		matches := o.FilenameRegex.FindStringSubmatch(fileName)

		// Если не нашли совпадений, то пропускаем файл
		if len(matches) < 2 {
			return nil, fmt.Errorf("invalid filename format: %s", fileName)
		}

		// matches[0] - полное совпадение
		versionStr, name := matches[1], matches[2]
		ver, err := time.Parse("2006-01-02_15-04", versionStr)
		if err != nil {
			return nil, err
		}

		// ioutil.ReadFile is deprecated, try to use os.Open instead
		script, err := os.Open(filepath.Join(o.Dir, fileName))
		if err != nil {
			return nil, err
		}
		defer script.Close()

		// Читаем содержимое файла в буфер
		var buffer bytes.Buffer
		_, err = buffer.ReadFrom(script)
		if err != nil {
			return nil, err
		}

		// Преобразуем содержимое буфера в строку
		fileContent := buffer.String()

		migrations = append(migrations, &Migration{
			Name:    name,
			Version: int(ver.Unix()),
			Script:  fileContent,
		})
	}

	sort.SliceStable(migrations, func(i, j int) bool {
		return migrations[i].Version < migrations[j].Version
	})

	return migrations, nil
}

// ----------------------------------------------------------------------------

// loader mock
type LoaderMock struct {
	LoadFunc func() ([]*Migration, error)
}

func (o *LoaderMock) Load() ([]*Migration, error) {
	return o.LoadFunc()
}
