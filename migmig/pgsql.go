package migmig

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

type PgMigrator struct {
	loader IMigLoader
}

func NewPgMigrator(ldr IMigLoader) *PgMigrator {
	return &PgMigrator{
		loader: ldr,
	}
}

func (o *PgMigrator) Apply(db *sql.DB) error {

	_, err := db.Exec(`
	CREATE TABLE IF NOT EXISTS migration_history (
		id SERIAL PRIMARY KEY,
		version INTEGER NOT NULL
	);`)
	if err != nil {
		return err
	}

	migrations, err := o.loader.Load()
	if err != nil {
		log.Println("Error loading migrations:", err)
		return err
	}

	for _, migration := range migrations {
		err := o.applyMigration(db, migration)
		if err != nil {
			return err
		}
	}

	return nil
}

func (o *PgMigrator) applyMigration(db *sql.DB, migration *Migration) error {
	fmt.Printf("Processing migration %s...\n", migration.Name)

	alreadyApplied, err := o.isMigrationApplied(db, migration)
	if err != nil {
		log.Println("Error checking migration history:", err)
		return err
	}

	if alreadyApplied {
		fmt.Printf("Migration %d already applied.\n", migration.Version)
		return nil
	}

	_, err = db.Exec(migration.Script)
	if err != nil {
		return err
	}

	_, err = db.Exec("INSERT INTO migration_history (version) VALUES ($1)", migration.Version)
	if err != nil {
		return err
	}

	return nil
}

func (o *PgMigrator) isMigrationApplied(db *sql.DB, migration *Migration) (bool, error) {
	var version int
	err := db.QueryRow("SELECT version FROM migration_history WHERE version = $1 LIMIT 1;", migration.Version).
		Scan(&version)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}

	return version == migration.Version, nil
}
