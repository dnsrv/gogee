package migmig

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoader(t *testing.T) {
	tests := loaderTests{}
	t.Run("getFiles", tests.TestGetFiles)
	t.Run("load", tests.load)
}

type loaderTests struct{}

func (o *loaderTests) TestGetFiles(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		dir := "testdata"

		loader := NewLoader(dir)
		assert.Equal(t, dir, loader.Dir)

		files, err := loader.GetFiles()
		assert.NoError(t, err)
		assert.Equal(t, 2, len(files))

		assert.Equal(t, "2019-03-20_00-00_descr_1.sql", files[0].Name())
		assert.Equal(t, "2019-03-20_00-01_descr_2.sql", files[1].Name())
	})
}

func (o loaderTests) load(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		dir := "testdata"

		loader := NewLoader(dir)
		assert.Equal(t, dir, loader.Dir)

		migrations, err := loader.Load()
		assert.NoError(t, err)
		assert.Equal(t, 2, len(migrations))

		expected := o.expectedMigrations()
		for i, migration := range migrations {
			assert.Equal(t, expected[i].Name, migration.Name)
			assert.Equal(t, expected[i].Version, migration.Version)
			assert.Equal(t, expected[i].Script, migration.Script)
		}
	})
}

func (o loaderTests) expectedMigrations() []*Migration {
	return []*Migration{
		{
			Name:    "descr_1",
			Version: 1553040000,
			Script:  "CREATE TABLE test (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL);",
		},
		{
			Name:    "descr_2",
			Version: 1553040060,
			Script:  "INSERT INTO test (name) VALUES ('test');",
		},
	}
}
