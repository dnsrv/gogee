package migmig

import (
	"database/sql"
	"testing"

	_ "modernc.org/sqlite"
)

func TestSqliteMig(t *testing.T) {
	db, err := sql.Open("sqlite", ":memory:")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	testSuite := sqliteMitTests{
		db: db,
	}

	t.Run("apply", testSuite.TestApply)
}

type sqliteMitTests struct {
	db *sql.DB
}

func (o *sqliteMitTests) TestApply(t *testing.T) {
	t.Run("success", func(t *testing.T) {

		loader := &LoaderMock{
			LoadFunc: func() ([]*Migration, error) {
				return []*Migration{
					{
						Name:    "2019-03-20_00-00",
						Version: 1,
						Script:  "CREATE TABLE test (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL);",
					},
					{
						Name:    "2019-03-20_00-01",
						Version: 2,
						Script:  "INSERT INTO test (name) VALUES ('test');",
					},
				}, nil
			},
		}

		migrator := NewSqliteMigrator(loader)

		err := migrator.Apply(o.db)
		if err != nil {
			t.Fatal(err)
		}

		var count int
		err = o.db.QueryRow("SELECT COUNT(*) FROM test").Scan(&count)
		if err != nil {
			t.Fatal(err)
		}

		if count != 1 {
			t.Fatalf("expected count 1, got %d", count)
		}
	})
}
