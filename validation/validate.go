package validation

import (
	"context"
	"sync"
)

type Rule func(ctx context.Context, item interface{}) error

func Validate(ctx context.Context, item interface{}, rules []Rule) error {

	if len(rules) == 0 {
		return nil
	}

	errChan := make(chan error)

	go func(chan error) {
		defer close(errChan)
		wg := sync.WaitGroup{}

		for _, rule := range rules {
			wg.Add(1)

			go func(wg *sync.WaitGroup, errChan chan error, rule Rule) {
				defer wg.Done()
				err := rule(ctx, item)
				if err != nil {
					errChan <- err
				}
			}(&wg, errChan, rule)
		}

		wg.Wait()
	}(errChan)

	errorsArr := make([]error, 0, len(rules))

	for err := range errChan {
		if err != nil {
			errorsArr = append(errorsArr, err)
		}
	}

	if len(errorsArr) > 0 {
		return errorsArr[0]
	}

	return nil
}
