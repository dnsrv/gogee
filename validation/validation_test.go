package validation

import (
	"context"
	"database/sql"
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

type TargetItem struct {
	Id   int
	Name string
}

func NewNameValidationRule() Rule {
	return func(ctx context.Context, item interface{}) error {

		target, ok := item.(*TargetItem)
		if !ok {
			return errors.New("invalid item type")
		}

		if target.Name == "" {
			return errors.New("empty name")
		}

		return nil
	}
}

func NewIdValidationRule() Rule {
	return func(ctx context.Context, item interface{}) error {

		target, ok := item.(*TargetItem)
		if !ok {
			return errors.New("invalid item type")
		}

		if target.Id <= 0 {
			return errors.New("invalid ID")
		}

		return nil
	}
}

// ----------------------------------------------------------------------------

func TestAddNodeValidationRules_tt(t *testing.T) {
	ctx := context.Background()

	testCases := []struct {
		name        string
		item        *TargetItem
		rule        Rule
		errExpected error
	}{
		{
			name: "valid name",
			item: &TargetItem{
				Name: "Dina",
			},
			rule:        NewNameValidationRule(),
			errExpected: nil,
		},
		{
			name: "empty name",
			item: &TargetItem{
				Name: "",
			},
			rule:        NewNameValidationRule(),
			errExpected: errors.New("empty name"),
		},
		{
			name: "valid ID",
			item: &TargetItem{
				Id: 0,
			},
			rule:        NewIdValidationRule(),
			errExpected: errors.New("invalid ID"),
		},
		{
			name: "invalid ID",
			item: &TargetItem{
				Id: 228,
			},
			rule:        NewIdValidationRule(),
			errExpected: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.rule(ctx, tc.item)
			assert.Equal(t, tc.errExpected, err)
		})
	}
}

func TestValidationSuccess(t *testing.T) {
	rule := func(ctx context.Context, item interface{}) error {
		return nil
	}

	err := Validate(context.Background(), nil, []Rule{rule})
	assert.Nil(t, err)
}

func TestValidationNoRules(t *testing.T) {
	err := Validate(context.Background(), nil, []Rule{})
	assert.Nil(t, err)
}

func TestValidation(t *testing.T) {
	errExpected := errors.New("error")
	rule := func(ctx context.Context, item interface{}) error {
		return errExpected
	}

	err := Validate(context.Background(), nil, []Rule{rule})
	assert.Equal(t, errExpected, err)
}

func TestValidationMoreRules(t *testing.T) {
	errExpected := errors.New("error")
	rule1 := func(ctx context.Context, item interface{}) error {
		return errExpected
	}

	rule2 := func(ctx context.Context, item interface{}) error {
		return sql.ErrConnDone
	}

	err := Validate(context.Background(), nil, []Rule{rule1, rule2})
	assert.True(t, err == errExpected || err == sql.ErrConnDone)
}

func TestValidationErrWithSuccess(t *testing.T) {
	rule1 := func(ctx context.Context, item interface{}) error {
		return nil
	}

	rule2 := func(ctx context.Context, item interface{}) error {
		return sql.ErrConnDone
	}

	err := Validate(context.Background(), nil, []Rule{rule1, rule2})
	assert.Equal(t, sql.ErrConnDone, err)
}