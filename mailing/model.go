package mailing

import (
	"crypto/md5"
	"crypto/tls"
	"fmt"
)

type Account struct {
	Host      string
	Port      int
	Username  string
	Password  string
	SSL       bool
	TLSConfig *tls.Config
}

func (o Account) GetHash() string {
	return fmt.Sprintf("%x", md5.Sum([]byte(o.Host+o.Username+o.Password)))[:8]
}

type Message struct {
	To      string
	Subject string
	Content *MessageContent
}

type MessageContent struct {
	Body string
	Type string
}
