package mailing

import (
	"fmt"

	"gopkg.in/gomail.v2"
)

type compositeSender struct {
	dialer *gomail.Dialer
	gomail.SendCloser
	account *Account
}

type Sender struct {
	senders    []*compositeSender
	nextSender int
	senderName string
	lh         ILogHandler
}

func NewSender(senderName string, accounts []*Account, lh ILogHandler) (*Sender, error) {
	sdr := &Sender{
		senders:    make([]*compositeSender, 0),
		senderName: senderName,
		lh:         lh,
	}

	for _, account := range accounts {
		d := gomail.NewDialer(account.Host, account.Port, account.Username, account.Password)
		d.TLSConfig = account.TLSConfig

		s, err := d.Dial()
		if err != nil {
			sdr.Close()
			return nil, err
		}

		sdr.senders = append(sdr.senders, &compositeSender{
			SendCloser: s,
			account:    account,
			dialer:     d,
		})
	}

	if len(sdr.senders) == 0 {
		return nil, fmt.Errorf("no senders available")
	}

	return sdr, nil
}

func (o *Sender) getSender() (*compositeSender, error) {
	for range o.senders {
		current := o.senders[o.nextSender]
		o.nextSender = (o.nextSender + 1) % len(o.senders)

		if current.SendCloser == nil {
			s, err := current.dialer.Dial()
			if err != nil {
				o.lh.Debug("SND002", fmt.Sprintf("Error dialing %s: %s", current.account.Username, err.Error()))
				current.SendCloser = nil
				continue
			}
			current.SendCloser = s
		}

		o.lh.Debug("SND001", fmt.Sprintf("Sender: %s", current.account.Username))
		return current, nil
	}

	return nil, fmt.Errorf("no online senders available")
}

func (o *Sender) Send(dm *Message) error {
	return o.send(dm, 1)
}

func (o *Sender) send(dm *Message, try int) error {
	if try > 3 {
		return fmt.Errorf("max send tries exceeded")
	}

	sender, err := o.getSender()
	if err != nil {
		return err
	}

	m := gomail.NewMessage()
	m.SetHeader("From", fmt.Sprintf("%s <%s>", o.senderName, sender.account.Username))
	m.SetHeader("To", dm.To)
	m.SetHeader("Subject", dm.Subject)
	m.SetBody(dm.Content.Type, dm.Content.Body)

	err = gomail.Send(sender, m)
	if err != nil {
		sender.SendCloser.Close()
		sender.SendCloser = nil
		return o.send(dm, try+1)
	}

	return nil
}

func (o *Sender) Close() error {
	var err error
	for _, sender := range o.senders {
		o.lh.Debug("SND003", fmt.Sprintf("Closing sender: %s", sender.account.Username))

		if sender.SendCloser == nil {
			continue
		}

		if e := sender.Close(); e != nil {
			o.lh.Debug("SND004", fmt.Sprintf("Error closing sender: %s", e.Error()))
			err = e
		}
	}
	return err
}
