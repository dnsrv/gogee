package mailing

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestContentBuilder(t *testing.T) {
	tests := contentBuilderTests{}

	tests.createTemplate("html1.html", t)
	tests.createTemplate("html2.html", t)
	tests.createTemplate("txt1.txt", t)

	t.Run("parsingTemplates", tests.parsingTemplates)
	t.Run("getExactContent", tests.getExactContent)
}

type contentBuilderTests struct{}

func (o contentBuilderTests) parsingTemplates(t *testing.T) {
	t.Run("parsingTemplates", func(t *testing.T) {
		dir := "testdata"
		cb, err := NewContentBuilder(dir, LogHandlerMock{})
		assert.NoError(t, err)

		assert.Equal(t, 3, len(cb.templates))
	})
}

func (o contentBuilderTests) getExactContent(t *testing.T) {
	t.Run("getExactContent", func(t *testing.T) {
		dir := "testdata"
		cb, err := NewContentBuilder(dir, LogHandlerMock{})
		assert.NoError(t, err)

		assert.Equal(t, 3, len(cb.templates))

		fmt.Printf(">>> templates: %+v\n", cb.templates)

		tmpl := cb.templates["html1.html"]
		assert.Equal(t, "text/html", tmpl.Type)
		assert.Equal(t, o.getContent("html1.html"), tmpl.Body)

		tmpl = cb.templates["html2.html"]
		assert.Equal(t, "text/html", tmpl.Type)
		assert.Equal(t, o.getContent("html2.html"), tmpl.Body)

		tmpl = cb.templates["txt1.txt"]
		assert.Equal(t, "text/plain", tmpl.Type)
		assert.Equal(t, o.getContent("txt1.txt"), tmpl.Body)
	})
}

func (o contentBuilderTests) createTemplate(key string, t *testing.T) {
	content := o.getContent(key)

	dir := "testdata"
	filename := key

	f, err := os.Create(dir + "/" + filename)
	assert.NoError(t, err)
	assert.NotNil(t, f)
	defer f.Close()

	_, err = f.WriteString(content)
	assert.NoError(t, err)
}

func (o contentBuilderTests) getContent(filename string) string {
	switch filename {
	case "html1.html":
		return `
			<p>This is HTML content of the Message 1</p>
		`
	case "html2.html":
		return `
			<p>This is HTML content of the Message 2</p>
		`
	case "txt1.txt":
		return `
			This is TXT content of the Message 1
		`
	}

	return ""
}

type LogHandlerMock struct {
}

func (o LogHandlerMock) Debug(code string, msg string) {
	fmt.Printf(">>> %s: %s\n", code, msg)
}
