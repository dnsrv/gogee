package mailing

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path"
	"text/template"
)

type ContentBuilder struct {
	templates map[string]Template
}

type Template struct {
	Type string // html|txt
	Body string // file content
}

const (
	TemplateTypeHTML = "text/html"
	TemplateTypeTXT  = "text/plain"
)

type ILogHandler interface {
	Debug(code string, msg string)
}

func NewContentBuilder(templatesDir string, lh ILogHandler) (*ContentBuilder, error) {
	lh.Debug("NCB001", fmt.Sprintf("Parsing templates in: %s", templatesDir))

	files, err := os.ReadDir(templatesDir)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	templates := make(map[string]Template)

	for _, file := range files {
		filename := file.Name()
		filepath := path.Join(templatesDir, filename)

		lh.Debug("NCB002", fmt.Sprintf("Parsing file: %s", filepath))

		f, err := os.Open(filepath)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
		defer f.Close()

		content, err := io.ReadAll(f)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}

		templates[filename] = Template{
			Type: GetTypeByExt(path.Ext(filename)),
			Body: string(content),
		}
	}

	return &ContentBuilder{
		templates: templates,
	}, nil
}

func GetTypeByExt(ext string) string {
	switch ext {
	case ".html":
		return TemplateTypeHTML
	case ".txt":
		return TemplateTypeTXT
	default:
		return ""
	}
}

func (o *ContentBuilder) GetContent(templateName string, data any) (*MessageContent, error) {
	tpl, ok := o.templates[templateName]
	if !ok {
		return nil, fmt.Errorf("template not found")
	}

	tmpl, err := template.New(templateName).Parse(tpl.Body)
	if err != nil {
		return nil, err
	}

	var tplBuff bytes.Buffer
	if err := tmpl.Execute(&tplBuff, data); err != nil {
		return nil, err
	}

	return &MessageContent{
		Type: tpl.Type,
		Body: tplBuff.String(),
	}, nil
}
