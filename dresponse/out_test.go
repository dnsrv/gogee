package dresponse

import (
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOut(t *testing.T) {
	tests := &outTestSuite{}
	t.Run("usage", tests.usage)
	t.Run("json", tests.json)
	t.Run("exec", tests.exec)
}

type outTestSuite struct {
	golden map[string][]byte
}

func (o *outTestSuite) usage(t *testing.T) {
	t.Run("usage", func(t *testing.T) {
		t.Parallel()

		out := New()
		assert.Equal(t, 200, out.HttpCode)

		assert.Equal(t, "", out.Error.Code)
		assert.Equal(t, "", out.Error.Message)
		assert.Equal(t, []string(nil), out.Error.Suggestions)

		assert.Equal(t, "", out.Meta.RequestId)
		assert.NotEqual(t, "", out.Meta.Timestamp)
		assert.Equal(t, "", out.Meta.Version)
		assert.Equal(t, "", out.Meta.Status)

		assert.Equal(t, &EmptyData{}, out.Data)

		out.WithHttpCode(200)
		assert.Equal(t, 200, out.HttpCode)

		out.WithError("code", assert.AnError)
		assert.Equal(t, "code", out.Error.Code)
		assert.Equal(t, assert.AnError.Error(), out.Error.Message)
		assert.Equal(t, []string(nil), out.Error.Suggestions)

		out.WithDebug("k1", "Debug error message")
		assert.Equal(t, "Debug error message", out.Meta.Debug["k1"])

	})
}

func (o *outTestSuite) json(t *testing.T) {
	o.loadData(t)

	t.Run("out-1", func(t *testing.T) {
		out := New()
		assert.Equal(t, &EmptyData{}, out.Data)

		out.WithHttpCode(555)
		out.WithError("XTE.5.1.1", errors.New("Public error message"))
		out.WithDebug("k1", "Debug error message")
		out.WithSuggestion("Suggestion 1").WithSuggestion("Suggestion 2")

		out.WithMeta(RequestID, "1234567890")
		out.WithMeta(Timestamp, "2023-08-24T23:22:18+04:00")
		out.WithMeta(Version, "v1.0.0")
		out.WithMeta(Status, "fail")

		outJsonBytes, err := json.Marshal(out)
		assert.NoError(t, err)
		t.Log(string(outJsonBytes))

		assert.JSONEq(t, string(o.golden["out-1.json"]), string(outJsonBytes))
	})
	t.Run("out-2", func(t *testing.T) {
		out := New()
		assert.Equal(t, &EmptyData{}, out.Data)

		out.WithHttpCode(200)
		out.WithData("data")
		out.WithMeta(Timestamp, "2023-08-24T23:22:18+04:00")

		outJsonBytes, err := json.Marshal(out)
		assert.NoError(t, err)

		t.Log(string(outJsonBytes))

		assert.JSONEq(t, string(o.golden["out-2.json"]), string(outJsonBytes))
	})
	t.Run("out-3", func(t *testing.T) {
		out := New()
		assert.Equal(t, &EmptyData{}, out.Data)

		out.WithHttpCode(200)
		out.WithData([]string{"a1", "a2", "a3"})
		out.WithMeta(Timestamp, "2023-08-24T23:22:18+04:00")

		outJsonBytes, err := json.Marshal(out)
		assert.NoError(t, err)

		t.Log(string(outJsonBytes))

		assert.JSONEq(t, string(o.golden["out-3.json"]), string(outJsonBytes))
	})
	t.Run("out-4", func(t *testing.T) {
		out := New()
		assert.Equal(t, &EmptyData{}, out.Data)

		out.WithHttpCode(200)
		out.WithData(map[string]interface{}{
			"key1": "value1",
			"key2": 123,
			"key3": []string{"a1", "a2", "a3"},
		})
		out.WithMeta(Timestamp, "2023-08-24T23:22:18+04:00")

		outJsonBytes, err := json.Marshal(out)
		assert.NoError(t, err)

		t.Log(string(outJsonBytes))

		assert.JSONEq(t, string(o.golden["out-4.json"]), string(outJsonBytes))
	})
	t.Run("marshalling error", func(t *testing.T) {
		out := New()
		assert.Equal(t, &EmptyData{}, out.Data)

		out.WithHttpCode(200)
		out.WithData(&FailingMarshaler{})

		responseWriter := &responseWriterMock{
			Hdrs: make(http.Header),
			onWrite: func(b []byte) (int, error) {
				return len(b), nil
			},
		}
		out.Exec(responseWriter)

		assert.Equal(t, "JPE010", out.Error.Code)
		assert.Equal(t, "json marshal error", out.Error.Message)
		assert.Equal(t, 500, out.HttpCode)
	})
}

type FailingMarshaler struct{}

func (f FailingMarshaler) MarshalJSON() ([]byte, error) {
	return nil, errors.New("mock marshal error")
}

func (o *outTestSuite) exec(t *testing.T) {
	t.Run("exec-1", func(t *testing.T) {
		out := New()
		assert.Equal(t, &EmptyData{}, out.Data)

		out.WithHttpCode(200)
		out.WithData("data")
		out.WithMeta(Timestamp, "2023-08-24T23:22:18+04:00")

		outJsonBytes, err := json.Marshal(out)
		assert.NoError(t, err)

		responseWriter := &responseWriterMock{
			Hdrs: make(http.Header),
			onWrite: func(b []byte) (int, error) {
				assert.JSONEq(t, string(outJsonBytes), string(b))
				return len(b), nil
			},
		}

		out.Exec(responseWriter)
		assert.Equal(t, "application/json", responseWriter.Hdrs.Get("Content-Type"))
	})
}

type responseWriterMock struct {
	ResponseWriter http.ResponseWriter
	Hdrs           http.Header
	onWrite        func(b []byte) (int, error)
	statusCode     int
}

func (o *responseWriterMock) Header() http.Header {
	return o.Hdrs
}

func (o *responseWriterMock) Write(b []byte) (int, error) {
	return o.onWrite(b)
}

func (o *responseWriterMock) WriteHeader(statusCode int) {
	o.statusCode = statusCode
}

// load all json files from 'testdata' into map[string][]byte
func (o *outTestSuite) loadData(t *testing.T) {
	t.Helper()
	o.golden = make(map[string][]byte)

	dir, err := os.ReadDir("testdata")
	if err != nil {
		t.Error(err)
	}

	for _, file := range dir {
		if file.IsDir() {
			continue
		}

		b, err := os.ReadFile("testdata/" + file.Name())
		if err != nil {
			t.Error(err)
		}

		o.golden[file.Name()] = b
	}
}
