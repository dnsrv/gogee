package dresponse

import (
	"encoding/json"
	"net/http"
	"time"
)

type MetaKey int

const (
	RequestID MetaKey = iota
	Timestamp
	Version
	Status
)

type Response struct {
	HttpCode int                    `json:"-"`
	Error    Error                  `json:"error"`
	Data     interface{}            `json:"data"`
	Meta     Meta                   `json:"meta"`
	Debug    map[string]interface{} `json:"debug,omitempty"`
}

type Error struct {
	Code        string   `json:"code,omitempty"`
	Message     string   `json:"message,omitempty"`
	Suggestions []string `json:"suggestions,omitempty"`
	Debug       string   `json:"debug,omitempty"`
}

type Meta struct {
	Timestamp string                 `json:"timestamp"`
	RequestId string                 `json:"request_id"`
	Version   string                 `json:"version"`
	Status    string                 `json:"status"`
	Debug     map[string]interface{} `json:"debug,omitempty"`
}

type EmptyData struct {
}

func New() *Response {
	return &Response{
		HttpCode: http.StatusOK,
		Meta: Meta{
			Timestamp: time.Now().Format(time.DateTime),
			RequestId: "",
			Version:   "",
			Status:    "",
		},
		Error: Error{},
		Data:  &EmptyData{},
	}
}

func (o *Response) WithHttpCode(code int) *Response {
	o.HttpCode = code
	return o
}

func (o *Response) WithError(code string, err error) *Response {
	o.Error.Code = code
	o.Error.Message = err.Error()
	o.Meta.Status = "fail"
	return o
}

func (o *Response) WithDebug(key string, value interface{}) *Response {
	if o.Meta.Debug == nil {
		o.Meta.Debug = make(map[string]interface{})
	}
	o.Meta.Debug[key] = value
	return o
}

func (o *Response) WithData(data interface{}) *Response {
	o.Data = data
	return o
}

func (o *Response) WithSuggestion(suggestion string) *Response {
	o.Error.Suggestions = append(o.Error.Suggestions, suggestion)
	return o
}

func (o *Response) WithMeta(key MetaKey, value string) *Response {
	switch key {
	case RequestID:
		o.Meta.RequestId = value
	case Timestamp:
		o.Meta.Timestamp = value
	case Version:
		o.Meta.Version = value
	case Status:
		o.Meta.Status = value
	}

	return o
}

func (o *Response) Exec(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")

	bytes, err := json.Marshal(o)
	if err != nil {
		o.Error.Code = "JPE010"
		o.Error.Message = "json marshal error"
		o.Meta.Status = "error"
		o.Data = nil
		o.Meta.Debug = map[string]interface{}{
			"json_marshal_error": err.Error(),
		}

		bytes, _ = json.Marshal(o)
		o.HttpCode = http.StatusInternalServerError
	}

	w.WriteHeader(o.HttpCode)
	w.Write(bytes)
}
