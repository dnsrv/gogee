package apperr

import "fmt"

type Error struct {
	PubErr error
	SysErr error
}

func NewError(pubMsg string) *Error {
	return &Error{
		PubErr: fmt.Errorf(pubMsg),
	}
}

func (o *Error) Error() string {
	return o.PubErr.Error()
}

func (o *Error) Unwrap() error {
	return o.PubErr
}

func (o *Error) WithSystemError(err error) *Error {
	o.SysErr = err
	return o
}

func (o *Error) WithPublicError(err error) *Error {
	o.PubErr = err
	return o
}

func (o *Error) IsPublicError() bool {
	return o.SysErr == nil
}

func (o *Error) IsPrivateError() bool {
	return o.SysErr != nil
}
